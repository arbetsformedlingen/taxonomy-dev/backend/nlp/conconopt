.PHONY: conconopt simple_optimization

CC = clang++
conconopt = build/conconopt

cpp_files = src/cpp/main.cpp src/cpp/TreeStateOptimizer.cpp src/cpp/SkosTerminology.cpp src/cpp/Conconopt.cpp src/cpp/TerminologyTree.cpp src/cpp/GaussianCost.cpp src/cpp/ReduceTree.cpp src/cpp/TextUI.cpp
hpp_files = src/cpp/TreeStateOptimizer.hpp  src/cpp/utils.hpp src/cpp/SkosTerminology.hpp src/cpp/Conconopt.hpp src/cpp/TerminologyTree.hpp	src/cpp/GaussianCost.hpp src/cpp/TreeStateOptimizationProblem.hpp src/cpp/ReduceTree.hpp src/cpp/TreeStateOptimizerParameterizedNode.hpp src/cpp/TextUI.hpp
cpp_sources = $(cpp_files) $(hpp_files)
py_src = src/py/conconopt/api.py
pyapi = poetry run python3 -c
occupation_jsonld = data/demo_occupation_sni/occupation.jsonld
sni_jsonld = data/demo_occupation_sni/sni.jsonld # This one needs to be downloaded *manually* from editera.dataportal.se
occupation_sni_sbert = data/demo_occupation_sni/occupation_sni_sbert.json

all: conconopt

update-json:
	rm external/include/nlohmann/json.hpp && wget --directory-prefix external/include/nlohmann https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp


# Library for reading and writing csv: https://github.com/vincentlaucsb/csv-parser
update-csv:
	rm -rf external/include/csv.hpp && wget --directory-prefix external/include https://raw.githubusercontent.com/vincentlaucsb/csv-parser/2.0.0-beta/single_include/csv.hpp

$(conconopt): $(cpp_sources)
	mkdir -p build && $(CC) -O2 -I external/include --std=c++17 $(cpp_files) -o build/conconopt

conconopt: $(conconopt)



#### DEMO Occupation -> SNI
data/demo_occupation_sni/occupation.ttl:
	wget --directory-prefix=data/demo_occupation_sni https://data.jobtechdev.se/taxonomy/skos/raw-ttl/occupation.ttl

$(occupation_jsonld): data/demo_occupation_sni/occupation.ttl $(py_src)
	$(pyapi) 'import conconopt.api as api; api.ConconoptApi().convert_rdf("data/demo_occupation_sni/occupation.ttl", "n3", "data/demo_occupation_sni/occupation.jsonld", "json-ld")'

$(occupation_sni_sbert): $(occupation_jsonld) $(sni_jsonld) $(py_src)
	$(pyapi) 'import conconopt.api as api; api.ConconoptApi().sbert_embed_labels(["data/demo_occupation_sni/occupation.jsonld", "data/demo_occupation_sni/sni.jsonld"], "data/demo_occupation_sni/occupation_sni_sbert.json")'





#### Data for unit tests
sample_data/sni_slice.jsonld: $(sni_jsonld)
	mkdir -p sample_data && $(pyapi) 'import conconopt.api as api; api.ConconoptApi().slice_json("data/demo_occupation_sni/sni.jsonld", 0, 100, "sample_data/sni_slice.jsonld")'



#### C++-stuff

test: $(conconopt)
	$(conconopt) test

simple_optimization: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json simple_optimization

accumulate: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json accumulate

analyze: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json analyze

sample: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json sample

reg_sweep: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json reg_sweep

sweep_condition_number: $(conconopt)
	$(conconopt) run data/demo_occupation_sni/config.json sweep_condition_number

plot_condition_number:
	$(pyapi) 'import conconopt.setup as setup; setup.Setup("data/demo_occupation_sni/config.json").perform_task("plot_condition_number")'

sweep_comparison:
	$(conconopt) run data/demo_occupation_sni/config.json sweep_comparison

edit:
	$(conconopt) run data/demo_occupation_sni/config.json edit

# Conconopt - Concept connection optimizer

Conconopt is a software for optimizing connections between concepts from different treestructured SKOS terminologies. It takes as input a source terminology and a destination terminology and outputs a set of connections from concepts in the source terminology to the destination terminology.

The underlying optimization problem consists of *data terms* and a *regularization term*. A data term can typically be constructed from cosine similarities between SBERT embeddings of the preferred labels from the two terminologies. The regularization term is constructed from the tree structures of the terminologies: for instance, if in the source terminology a concept A has parent concept B and in the destination terminology a concept X has concept Y, it means that if we connect A and X, this connection encourages a connection B and Y.

## Solution space

Suppose without loss of generality that the source terminology has `m` states indexed from 0 to `m-1` and the destination terminology has `n` states indexed from `0` to `n-1`.

A vector `X` of integers of dimension `m` states in the source terminology where every element `x_i` is the corresponding node in the target terminology.

## The Data term

The data term can be computed from various sources such as

* SBERT label embeddings
* Statistics
* String matching
* Other stuff

## Regularization term

Every transition cost with child mapping to state `x_i` and parent mapping to node `x_j` is 0 if the mapped node `x_i` has an ancestor `x_j`, otherwise it is 1.

By evaluating the regularization term to, say, R(X), we can compute a percentage (1 - R(X))/n_e with `n_e` being the number of edges. This reflects how well the solution fits with the target tree.

Also, once we get the solution, every edge will either have 1 or 0 cost. This lets us split up the source tree into subtrees and for every source node, we can present the size of the subtree that it belongs to.

### Tuning the regularization term

* *Roughly*: Choose for instance the average data cost (use the `analyze_problem` task for that)
* Other heuristics:
  1. Accumulate the problem for various regularization weights.
  2. For every accumulation, sample random solutions from noise on the data costs.
  3. Pick the the solution that deviates the most from the optimum. The number of states that differ is like a [*condition number*](https://en.wikipedia.org/wiki/Condition_number).
  4. Pick the regularization weight that results in the lowest condition number (meaning "most stable solution").

## Generating multiple candidate destination concepts

Solve the problem multiple times, adding random noise to the data term every time. The standard deviation of the noise can be the standard deviation of the K best candidates for every source context. Build histograms of the best candidations for every source node. Present them in a table.

## Regularization parameter sweep

Solve the problem multiple times with different regularization parameter values. Present the mapped concepts in a table, with the concepts that change the most highest up...

## Matching in both directions

Start by solving from source to destination. Review the solution and tweak it. Use the solution connections as constraints and solve in the opposite direction for the nodes that have not been assigned. Review the opposite solution.

## Interactive web frontend

Make it possible to interact with the solution on a web page. To make this work really smoothly, a user update only has to update the path to the root from the node that was assigned, if it was assigned to something different.

## Running the code

See the Dockerfile for details.

But if you want to run it on your computer:

* Clang++ compiler
* Make

## Extending to multiple candidates per source concept

Once a single candidate has been established to every concept, we solve the problem again but with penalty costs on the established links in order encourage new candidates (for that, we may have to tweak the ParentChildRegCost term to accept old parents as well). This will result in possibly different solution with additional candidates (or the same). This process can be repeated until convergence or for a fixed number of steps...

## Exploring individual solutions constrained by single assignments

Generate candidates for every source state by measuring the objective function value given that the source state is assigned to every destination state. To re-solve the problem we only need to tweak the solution a bit.

## To do

* Look into docker-lsp for Emacs.
* Visa regularisering för ett delträd.
* Straffa eller uppmuntra specifika mappning
* Fuzzy-flagga: Uppmuntra även föräldrar/barn (antal nivåer anges)

## Papers

**Approaches that do Viterbi (dynamic programming) on trees:**

* [3d pictorial structures for multiple view articulated pose estimation](http://openaccess.thecvf.com/content_cvpr_2013/papers/Burenius_3D_Pictorial_Structures_2013_CVPR_paper.pdf)
* [Articulated pose estimation with flexible mixtures-of-parts](https://vision.ics.uci.edu/papers/YangR_CVPR_2011/YangR_CVPR_2011.pdf). See section 4 for how dynamic programming works.

**Papers on Ontology matching/alignment**

* Good overview on the topic: [Ontology matching: state of the art and future challenges](https://hal.inria.fr/hal-00917910/document)
* Another survey: [Ontology matching: A literature review](http://dit.unitn.it/~p2p/RelatedWork/Matching/Cerdeira-Ontology%20Matching-2015.pdf). 
* [CODI: Combinatorial Optimization for Data Integration – Results for OAEI 2010](http://disi.unitn.it/~p2p/OM-2010/oaei10_paper4.pdf)

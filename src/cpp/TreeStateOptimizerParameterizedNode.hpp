/*
 * TreeStateOptimizerParameterizedNode.hpp
 *
 *  Created on: Nov 12, 2022
 *      Author: jonas
 */

#ifndef TREESTATEOPTIMIZERPARAMETERIZEDNODE_HPP_
#define TREESTATEOPTIMIZERPARAMETERIZEDNODE_HPP_

#include "TreeStateOptimizer.hpp"
#include "ReduceTree.hpp"

namespace ccopt {
namespace TreeStateOptimizerImpl {


//SolutionForState() {}
//SolutionForState(int si) : stateIndex(si) {}

template <typename T>
struct Min {
  T operator()(const T& a, const T& b) const {
    return std::min(a, b);
  }
};

template <typename Cost>
std::optional<Cost> addOpt(const std::optional<Cost>& a, const std::optional<Cost>& b) {
  return (bool(a) && bool(b))? std::optional<Cost>(a.value() + b.value()) : std::optional<Cost>();
}

template <typename Cost>
struct ParameterizedState {
  typedef ReduceTree<StateCost<int, Cost>, Min<StateCost<int, Cost>>> ChildStateCostTree;

  bool debug = false;
  StateIndex stateIndex;
  std::optional<Cost> lastTotalCost = Cost(0);
  std::optional<Cost> baseCost = Cost(0);
  Cost originalBaseCost = Cost(0);
  Cost costForOtherChildren = Cost(0);
  ChildStateCostTree childStateCostTree;


  ParameterizedState(ITreeStateOptimizationProblem<Cost>* problem,
      const SolutionForNode<Cost>& sol,
      Cost bc,
      const SolutionForState<Cost>& state, int poChildIndex, const SolutionForNode<Cost>* childSol)
    : originalBaseCost(bc), baseCost(bc), stateIndex(state.stateIndex) {
    NodeIndex nodeIndex = sol.nodeIndex;
    debug = sol.debug;
    std::unordered_set<int> poChildSet(sol.internalChildInds.begin(), sol.internalChildInds.end());
    CHECK(poChildIndex == -1 || poChildSet.count(poChildIndex) == 1);

    for (size_t j = 0; j < state.costsPerChild.size(); j++) {
      const auto& childCost = state.costsPerChild[j];
      int poThisChildIndex = sol.internalChildInds[j];
      if (poThisChildIndex == poChildIndex) {
        //originalChildCost = c.cost;
        int stateCount = childSol->solutionPerState.size();
        std::vector<std::optional<StateCost<int, Cost>>> costPerState(stateCount);
        for (size_t i = 0; i < stateCount; i++) {
          //CRASH("ADD transition cost here");
          const auto& ss = childSol->solutionPerState[i];
          if (ss.enabled) {

            auto transitionCost = problem->getTransitionCost(childSol->nodeIndex, nodeIndex, ss.stateIndex, stateIndex);

            auto sum = ss.totalCost + transitionCost;
            costPerState[i] = StateCost<int, Cost>(i, sum);
          } else {
            // Let cost per state be undefined.
          }
        }
        childStateCostTree = ChildStateCostTree(costPerState);
        {
          auto top = childStateCostTree.root();
          if (top) {
            if (!(top.value().cost == childCost.cost)) {
              LOG("WARNING!!! Inconsistent costs");
              DOUT(top.value().cost);
              DOUT(childCost.cost);
            }
          }
        }
      } else  {
        costForOtherChildren = costForOtherChildren + childCost.cost;
      }
    }
    setStateEnabled(state.enabled);
    reset();
  }

  void setBaseCost(Cost bc) {
    baseCost = bc;
  }

  void setStateEnabled(bool e) {
    baseCost = e? std::optional<Cost>(originalBaseCost) : std::optional<Cost>();
  }

  void setChildStateCost(int childStateInternalIndex, const std::optional<Cost>& c) {
    childStateCostTree.set(
        childStateInternalIndex,
        bool(c)?
            std::optional<StateCost<int, Cost>>(StateCost<int, Cost>(childStateInternalIndex, c.value()))
            :
            std::optional<StateCost<int, Cost>>());
  }

  std::optional<Cost> totalCost() const {
    if (!bool(baseCost)) {
      return {};
    }
    Cost c = baseCost.value() + costForOtherChildren;
    if (childStateCostTree.empty()) {
      return c;
    }

    auto top = childStateCostTree.root();
    if (!bool(top)) {
      return {};
    }

    return c + top.value().cost;

  }

  void reset() {
    lastTotalCost = totalCost();
  }

  bool hasChanged() const {
    return totalCost() != lastTotalCost;
  }

  struct DiffData {
    bool equal = true;
    double difference = 0.0;
    double relativeDifference = 0.0;
  };

  DiffData diff() const {
    auto total = totalCost();

    DiffData dst;
    if (!bool(lastTotalCost) && !bool(total)) {
      return dst;
    } else if (bool(lastTotalCost) && bool(total)) {
      dst.equal = (lastTotalCost == total);
      double a = double(lastTotalCost.value());
      double b = double(total.value());
      dst.difference = b - a;
      dst.relativeDifference = dst.difference/std::max(std::abs(a), std::abs(b));
      return dst;
    } else {
      dst.equal = false;
      dst.difference = 1.0;
      dst.relativeDifference = 1.0;
      return dst;
    }
  }
};

//Cost totalCost = Cost(std::numeric_limits<double>::infinity());
//std::vector<StateCost<Cost>> costsPerChild;

template <typename Cost>
class ParameterizedNode {
public:
  ParameterizedNode(
      ITreeStateOptimizationProblem<Cost>* problem,
      TreeStateOptimizationAccumulation<Cost>* acc,
      int poChildIndex, int thisIndex) :
    _acc(acc), _childIndex(poChildIndex), _thisIndex(thisIndex) {
    const auto& sol = acc->postorder()[thisIndex];
    _debug = sol.debug;
    for (const auto& state: sol.solutionPerState) {
      auto baseCost = problem->getStateCost(sol.nodeIndex, state.stateIndex);
      _stateMap[state.stateIndex] = _states.size();
      _states.push_back(ParameterizedState<Cost>(problem, sol, baseCost, state, poChildIndex, poChildIndex == -1? nullptr : &(acc->postorder()[poChildIndex])));
    }
  }

  template <typename Visitor>
  void visitStates(const Visitor& visitor) {
    for (size_t i = 0; i < _states.size(); i++) {
      auto statePtr = &(_states[i]);
      visitor(i, statePtr);
      if (statePtr->hasChanged()) {
        _updatedStates.insert(i);
      }
    }
  }

//  void setStateBaseCost(int internalStateIndex, Cost c) {
//    _states[internalStateIndex].setBaseCost(c);
//  }

  // returns the internal state index
  int backTrace(int internalStateIndex) const {
    const auto& state = _states[internalStateIndex];
    auto root = state.childStateCostTree.root();
    if (root) {
      return root.value().state;
    } else {
      return -1;
    }
  }

  StateIndex getTrueStateIndex(int i) const {
    return _states[i].stateIndex;
  }

  int getInternalStateIndex(StateIndex i) const {
    return _stateMap.at(i);
  }

  const std::vector<ParameterizedState<Cost>>& getStates() const {
    return _states;
  }

  void reset() {
    _updatedStates = {};
    for (auto& s: _states) {
      s.reset();
    }
  }

  bool hasChanged() const {
    for (const auto& s: _states) {
      if (s.hasChanged()) {
        return true;
      }
    }
    return false;
  }

  StateCost<StateIndex, Cost> minimize() const {
    StateCost<StateIndex, Cost> opt;
    for (const auto& s: _states) {
      auto tc = s.totalCost();
      if (bool(tc)) {
        opt = std::min(opt, StateCost<StateIndex, Cost>(s.stateIndex, tc.value()));
      }
    }
    return opt;
  }

  NodeIndex getTrueIndex(int i) const {
    return _acc->postorder()[i].nodeIndex;
  }

  void recompute(ITreeStateOptimizationProblem<Cost>* problem, const ParameterizedNode<Cost>& child) {
    int counter = 0;
    for (size_t i = 0; i < _states.size(); i++) {
      auto& thisState = _states[i];
      for (int childStateIndex: child._updatedStates) {
        counter++;
        auto& childState = child._states[childStateIndex];
        thisState.setChildStateCost(childStateIndex, addOpt<Cost>(childState.totalCost(), problem->getTransitionCost(
            getTrueIndex(child._thisIndex), getTrueIndex(_thisIndex),
            childState.stateIndex, thisState.stateIndex)));
      }
      if (thisState.hasChanged()) {
        _updatedStates.insert(i);
      }
    }
    //LOG("     " << counter << " iterations, states = " << _states.size());
  }

  const std::unordered_set<int>& updatedStates() const {
    return _updatedStates;
  }
private:
  bool _debug = false;
  std::unordered_set<int> _updatedStates;
  std::unordered_map<StateIndex, int> _stateMap;
  TreeStateOptimizationAccumulation<Cost>* _acc;
  std::vector<ParameterizedState<Cost>> _states;
  int _childIndex;
  int _thisIndex;
};
}

template <typename Cost>
class ParameterizeForNode {
public:
  ParameterizeForNode(
      ITreeStateOptimizationProblem<Cost>* problem,
      TreeStateOptimizationAccumulation<Cost>* acc, NodeIndex nodeIndex) : _acc(acc) {
    int index = _acc->nodeArrayIndexMap().at(nodeIndex);
    int childIndex = -1;
    while (index != -1) {
      const auto& node = acc->postorder()[index];
      if (childIndex != -1) {
        CHECK(acc->postorder()[childIndex].parentIndex == index);
      }
      _parameterizedNodes.push_back(TreeStateOptimizerImpl::ParameterizedNode<Cost>(problem, acc, childIndex, index));
      childIndex = index;
      index = node.parentIndex;
    }
  }

  template <typename Visitor>
  void update(ITreeStateOptimizationProblem<Cost>* problem, const Visitor& v) {
    for (auto& p: _parameterizedNodes) {
      p.reset();
    }
    for (const auto& p: _parameterizedNodes) {
      CHECK(!p.hasChanged());
    }
    //LOG("INITIALIZE at 0");
    _parameterizedNodes[0].visitStates(v);
    for (size_t i = 1; i < _parameterizedNodes.size(); i++) {
      //LOG("REcompute at " << i);
      _parameterizedNodes[i].recompute(problem, _parameterizedNodes[i-1]);
    }
  }

  int totalUpdatedStateCount() const {
    int k = 0;
    for (const auto& p: _parameterizedNodes) {
      k += p.updatedStates().size();
    }
    return k;
  }

  void setState(ITreeStateOptimizationProblem<Cost>* problem, StateIndex stateIndex) {
    update(problem, [=](int internalIndex, TreeStateOptimizerImpl::ParameterizedState<Cost>* s) {
      s->setStateEnabled(s->stateIndex == stateIndex);
    });
  }

  void enableAll(ITreeStateOptimizationProblem<Cost>* problem) {
    update(problem, [](int internalIndex, TreeStateOptimizerImpl::ParameterizedState<Cost>* s) {
      s->setStateEnabled(true);
    });
  }

  void disableState(ITreeStateOptimizationProblem<Cost>* problem, StateIndex stateIndex) {
    update(problem, [=](int internalIndex, TreeStateOptimizerImpl::ParameterizedState<Cost>* s) {
      if (s->stateIndex == stateIndex) {
        s->setStateEnabled(false);
      }
    });
  }

  TreeStateOptimizerImpl::StateCost<StateIndex, Cost> minimizeTop() const {
    return _parameterizedNodes.back().minimize();
  }

  StateIndex traceBottomStateIndex(StateIndex topStateIndex) const {
    auto init = minimizeTop();
    size_t nodeCount = _parameterizedNodes.size();
    int index = _parameterizedNodes.back().getInternalStateIndex(init.state);
    for (size_t i = 0; i < nodeCount-1; i++) {
      index = _parameterizedNodes[nodeCount-1-i].backTrace(index);
      CHECK(index != -1);
    }
    return _parameterizedNodes[0].getTrueStateIndex(index);
  }

  TreeStateOptimizerImpl::StateCost<StateIndex, Cost> minimizeBottom() const {
    auto x = minimizeTop();
    return {traceBottomStateIndex(x.state), x.cost};

  }

  const std::vector<TreeStateOptimizerImpl::ParameterizedNode<Cost>>& getParameterizedNodes() const {
    return _parameterizedNodes;
  }
private:
  std::vector<TreeStateOptimizerImpl::ParameterizedNode<Cost>> _parameterizedNodes;
  TreeStateOptimizationAccumulation<Cost>* _acc;
};

}



#endif /* TREESTATEOPTIMIZERPARAMETERIZEDNODE_HPP_ */

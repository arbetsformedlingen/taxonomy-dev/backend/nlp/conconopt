#include <iostream>
#include "SkosTerminology.hpp"
#include "utils.hpp"
#include <nlohmann/json.hpp>
#include <fstream>
#include <set>
#include <filesystem>

namespace ccopt {

namespace {
  std::set<std::string> getTypes(const nlohmann::json& x) {
    auto ftype = x.find("@type");
    if (ftype != x.end()) {
      std::set<std::string> types;
      for (const auto& t: *ftype) {
        std::string ts = t;
        types.insert(ts);
      }
      return types;
    }
    return {};
  }

  std::string conceptType = "http://www.w3.org/2004/02/skos/core#Concept";

  bool isConcept(const nlohmann::json& x) {
    return getTypes(x).count(conceptType) == 1;
  }

  bool isConceptOrUndefined(const nlohmann::json& x) {
    auto t = getTypes(x);
    return t.empty() || t.count(conceptType) == 1;

  }

  std::vector<LangString> readLabels(const nlohmann::json& x, const std::string& k) {
    auto f = x.find(k);
    if (f == x.end()) {
      return {};
    }
    auto src = *f;

    std::vector<LangString> dst;
    for (const auto& label: src) {
      dst.push_back(LangString(label["@value"], label["@language"]));
    }
    return dst;
  }

  std::vector<SkosKey> readRelations(const nlohmann::json& src, const std::string& k) {
    auto f = src.find(k);
    if (f == src.end()) {
      return {};
    }

    std::vector<SkosKey> dst;
    for (const auto& rel: *f) {
      dst.push_back(SkosConcept::makeKey(rel["@id"]));
    }
    return dst;
  }
  
  SkosConcept parseSkosConcept(const nlohmann::json& x) {
    auto altLabels = readLabels(x, "http://www.w3.org/2004/02/skos/core#altLabel");
    auto prefLabels = readLabels(x, "http://www.w3.org/2004/02/skos/core#prefLabel");
    auto broader = readRelations(x, "http://www.w3.org/2004/02/skos/core#broader");
    auto narrower = readRelations(x, "http://www.w3.org/2004/02/skos/core#narrower");

    auto key = SkosConcept::makeKey(x["@id"]);
    return SkosConcept(key, prefLabels, altLabels, broader, narrower);
  }
}

const KeyType* KeyType::BASE = KeyTypeFactory::instance()->make("base", nullptr);
const KeyType* SkosConcept::CONCEPT_KEY_TYPE = KeyTypeFactory::instance()->make("Concept", KeyType::BASE);

KeyType* KeyTypeFactory::make(const std::string& id, const KeyType* parent) {
  CHECK(id.find(':') == id.npos);
  std::lock_guard<std::mutex> lock(_m);
  auto f = _registry.find(id);
  if (f == _registry.end()) {
    auto k = std::make_shared<KeyType>(id, parent);
    _registry[id] = k;
    return k.get();
  } else {
    auto k = f->second;
    CHECK(k->id() == id);
    CHECK(k->parent() == parent);
    return k.get();
  }
}

KeyType* KeyTypeFactory::find(const std::string& id) {
  std::lock_guard<std::mutex> lock(_m);
  auto f = _registry.find(id);
  return f == _registry.end()? nullptr : f->second.get();
}

std::map<std::string, std::shared_ptr<KeyType>> KeyTypeFactory::getRegistry() {
  std::lock_guard<std::mutex> lock(_m);
  return _registry;
}

KeyTypeFactory* KeyTypeFactory::instance() {
  static KeyTypeFactory instance;
  return &instance;
}

namespace {
  void setBroader(
    const std::set<SkosKey>& keySet,
    std::map<SkosKey, SkosKey>* dst,
    const SkosKey& child, const SkosKey& parent) {

    if (keySet.count(child) == 0) {
      std::cerr << "Missing child " << child << std::endl;
      return;
    }
    if (keySet.count(parent) == 0) {
      std::cerr << "Missing parent " << parent << std::endl;
      return;
    }
    
    auto f = dst->find(child);
    if (f == dst->end()) {
      (*dst)[child] = parent;
    } else if (f->second != parent) {
      LOG(f->second);
      LOG(parent);
      CRASH("Multiple broader concepts of " << child);
    }
  }

  std::vector<SkosKey> getBroader(const std::map<SkosKey, SkosKey>& src, const SkosKey& k) {
    auto f = src.find(k);
    return f == src.end()? std::vector<SkosKey>() : std::vector<SkosKey>{f->second};
  }

  std::vector<SkosKey> getNarrower(const std::map<SkosKey, std::vector<SkosKey>>& src, const SkosKey& k) {
    auto f = src.find(k);
    return f == src.end()? std::vector<SkosKey>() : f->second;
  }
}  
  
SkosTerminology SkosTerminology::fromConcepts(const std::vector<SkosConcept>& concepts) {
  std::set<SkosKey> keySet;
  for (const auto& c: concepts) {
    keySet.insert(c.key());
  }
  
  // Build a map pointing at broader concepts
  std::map<SkosKey, SkosKey> broaderMap;
  for (const auto& c: concepts) {
    for (const auto& narrower: c.narrower()) {
      setBroader(keySet, &broaderMap, narrower, c.key());
    }
    for (const auto& broader: c.broader()) {
      setBroader(keySet, &broaderMap, c.key(), broader);
    }
  }

  // Build narrowerMap from broaderMap
  std::map<SkosKey, std::vector<SkosKey>> narrowerMap;
  for (const auto& kv: broaderMap) {
    auto f = narrowerMap.find(kv.second);
    if (f == narrowerMap.end()) {
      narrowerMap[kv.second] = {kv.first};
    } else {
      f->second.push_back(kv.first);
    }
  }

  std::vector<SkosConcept> rebuiltConcepts;
  for (const auto& c: concepts) {
    rebuiltConcepts.push_back(
      c.withBroader(getBroader(broaderMap, c.key()))
      .withNarrower(getNarrower(narrowerMap, c.key())));
  }

  return SkosTerminology(rebuiltConcepts);
}

const SkosTerminology::ConceptData& SkosTerminology::operator[](const SkosKey& k) const {
  auto f = _conceptMap.find(k);
  CHECK(f != _conceptMap.end());
  return f->second;
}

const SkosTerminology::ConceptData* SkosTerminology::find(const SkosKey& k) const {
  auto f = _conceptMap.find(k);
  return f == _conceptMap.end()? nullptr : &(f->second);
}

namespace {
  int computeDepth(const std::map<SkosKey, SkosTerminology::ConceptData>& m, const SkosKey& k0) {
    std::set<SkosKey> visited;
    std::vector<SkosKey> visitedSeq;
    SkosKey k = k0;
    int depth = 0;
    while (true) {
      visitedSeq.push_back(k);
      const auto& cdata = m.at(k);
      if (0 < visited.count(k)) {
        LOG("CYCLE");
        for (auto v: visitedSeq) {
          LOG(v);
        }
        CRASH("Cycle detected at " << k);
      }
      visited.insert(k);
      const auto& b = cdata.concept.broader();
      if (b.empty()) {
        return depth;
      }

      k = b[0];
      depth++;
    }
  }
}

std::string serializeSkosKey(const SkosKey& src) {
  return src.type()->id() + ":" + src.id();
}
SkosKey deserializeSkosKey(const std::string& s) {
  auto sep = s.find(':');
  if (sep == s.npos) {
    return SkosKey();
  }
  auto typeId = s.substr(0, sep);
  auto id = s.substr(1 + sep);
  auto type = KeyTypeFactory::instance()->find(typeId);
  return SkosKey(type, id);
}


LangString findLabelInLanguage(const std::vector<LangString>& labels, const std::string& lang) {
  if (labels.empty()) {
    return LangString();
  }
  for (const auto& label: labels) {
    if (label.lang() == lang) {
      return label;
    }
  }
  return labels.front();
}
  
SkosTerminology::SkosTerminology(const std::vector<SkosConcept>& concepts) {
  for (const auto& c: concepts) {
    ConceptData cdata;
    cdata.concept = c;
    _conceptMap[c.key()] = cdata;
    _conceptKeys.push_back(c.key());
    if (c.broader().empty()) {
      _rootKeys.push_back(c.key());
    }
    if (c.narrower().empty()) {
      _leafKeys.push_back(c.key());
    }
  }
  for (auto& kv: _conceptMap) {
    auto& cdata = kv.second;
    cdata.depth = computeDepth(_conceptMap, kv.first);
  }
}

SkosConcept parseTopLevelSkosConcept(const nlohmann::json& item) {
  auto fgraph = item.find("@graph");
  if (fgraph != item.end()) {
    auto graph = *fgraph;
    for (const auto& graphItem: graph) {
      if (isConcept(graphItem)) {
        return parseSkosConcept(graphItem);
      }
    }
  } else if (0 < item.count("@id") && isConceptOrUndefined(item)) {
    return parseSkosConcept(item);
  }
  return SkosConcept();
}
  
SkosTerminology SkosTerminology::loadFromJsonld(const std::string& filename) {
  CHECK(std::filesystem::exists(filename));

  std::ifstream file(filename);
  auto data = nlohmann::json::parse(file);

  LOG("Data size " << data.size());

  std::vector<SkosConcept> concepts;

  std::vector<nlohmann::json> rejected;
  for (const auto& item: data) {
    auto concept = parseTopLevelSkosConcept(item);
    if (concept.isDefined()) {
      concepts.push_back(concept);
    } else {
      rejected.push_back(item);
    }
  }
  int totalCount = concepts.size() + rejected.size();
  LOG("Number of concepts loaded: " << concepts.size() << "/" << totalCount);

  return SkosTerminology(SkosTerminology::fromConcepts(concepts));
}

void testSkosTerminology() {
  {
    CHECK(KeyType::BASE->hasSubType(SkosConcept::CONCEPT_KEY_TYPE));
    CHECK(!(SkosConcept::CONCEPT_KEY_TYPE->hasSubType(KeyType::BASE)));
    CHECK(!KeyType::BASE->hasSubType(nullptr));
  }{
    auto sni = SkosTerminology::loadFromJsonld("sample_data/sni_slice.jsonld");
    const auto& ks = sni.conceptKeys();
    CHECK(0 < ks.size());
    CHECK(0 < sni.rootKeys().size());
    CHECK(0 < sni.leafKeys().size());
    CHECK(0 == sni[sni.rootKeys()[0]].depth);

    for (auto k: sni.rootKeys()) {
      auto cdata = sni[k];
      CHECK(cdata.depth == 0);
    }

    for (auto k: sni.conceptKeys()) {
      auto cdata = sni[k];
      CHECK((cdata.depth == 0) == cdata.concept.broader().empty());
    }
  }{
    auto a = KeyTypeFactory::instance()->make("a");
    auto b = KeyTypeFactory::instance()->make("b");
    auto a2 = KeyTypeFactory::instance()->make("a");
    CHECK(a == a2);
    CHECK(a != b);

    auto k = SkosKey(a, "Mjao");
    auto s = serializeSkosKey(k);
    CHECK(s == "a:Mjao");
    auto k2 = deserializeSkosKey(s);
    CHECK(k2.isDefined());
    //DOUT(k2.type()->id());
    //DOUT(k2.id());
    CHECK(k == k2);
  }
}
  
}

std::ostream& operator<<(std::ostream& s, const ccopt::LangString& x) {
  s << "'" << x.value() << "'(" << x.lang() << ")";
  return s;
}

std::ostream& operator<<(std::ostream& s, const ccopt::SkosKey& k) {
  s << k.type()->id() << ":" << k.id();
  return s;
}


namespace {

  template <typename T>
  void vectorToOstream(std::ostream* dst, const std::vector<T>& s) {
    *dst << "[";
    bool sep = false;
    for (const auto& x: s) {
      if (sep) {
        *dst << ", ";
      }
      *dst << x;
      sep = true;
    }
    *dst << "]";
  }
  
}

std::ostream& operator<<(std::ostream& s, const ccopt::SkosConcept& c) {
  s << "SkosConcept(key=" << c.key() << ", prefLabels=";
  vectorToOstream(&s, c.prefLabels());
  s << ", altLabels=";
  vectorToOstream(&s, c.altLabels());
  s << ", broader=";
  vectorToOstream(&s, c.broader());
  s << ", narrower=";
  vectorToOstream(&s, c.narrower());
  s << ")";
  return s;
}

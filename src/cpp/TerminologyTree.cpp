#include <iostream>
#include "TerminologyTree.hpp"

namespace ccopt {

  KeyType* TerminologyTreeNode::rootKeyType() {
    return KeyTypeFactory::instance()->make("rootNode", KeyType::BASE);
  }

  KeyType* TerminologyTreeNode::childNodeKeyType() {
    return KeyTypeFactory::instance()->make("childNodeOf", KeyType::BASE);
  }

  SkosKey TerminologyTreeNode::childNodeKey(const std::string& id) {
    return SkosKey(childNodeKeyType(), id);
  }

  SkosKey TerminologyTreeNode::rootKey() {
    return SkosKey(rootKeyType());
  }

  std::vector<TerminologyTreeNode::Ptr> treeNodesFromKeys(const TerminologySetup& setup, const std::vector<SkosKey>& ks);

  TerminologyTreeNode::Ptr treeNodeFromConcept(const TerminologySetup& setup, const SkosConcept& c) {
    auto nodes = treeNodesFromKeys(setup, c.narrower());
    if (setup.addUndefinedChildNodes) {
      auto k = TerminologyTreeNode::childNodeKey(c.key().id());
      nodes.push_back(std::make_shared<TerminologyTreeNode>(k, std::vector<TerminologyTreeNode::Ptr>{}));
    }
    return std::make_shared<TerminologyTreeNode>(c.key(), nodes);
  }

  std::vector<TerminologyTreeNode::Ptr> treeNodesFromKeys(const TerminologySetup& setup, const std::vector<SkosKey>& ks) {
    std::vector<TerminologyTreeNode::Ptr> dst;
    for (const auto& k: ks) {
      dst.push_back(treeNodeFromConcept(setup, setup.terminology[k].concept));
    }
    return dst;
  }

  TerminologyTreeNode::Ptr TerminologySetup::buildTree() const {
    const auto& rootKeys = terminology.rootKeys();
    if (addRoot) {
      return std::make_shared<TerminologyTreeNode>(TerminologyTreeNode::rootKey(), treeNodesFromKeys(*this, rootKeys));
    } else {
      CHECK(1 == rootKeys.size());
      return treeNodesFromKeys(*this, rootKeys)[0];
    }
  }

  namespace {
    struct TreeNodeStats {
      int innerNodeCount = 0;
      int leafNodeCount = 0;
    };

    void traverseTerminology(const SkosKey& k, const SkosTerminology& terminology, std::vector<TreeNodeStats>* stats) {
      auto c = terminology[k];
      while (stats->size() < c.depth + 1) {
        stats->push_back(TreeNodeStats());
      }
      auto& at = (*stats)[c.depth];
      (*(c.concept.narrower().empty()? (&at.leafNodeCount) : (&at.innerNodeCount)))++;
      for (const auto& child: c.concept.narrower()) {
        traverseTerminology(child, terminology, stats);
      }
    }
  }

  nlohmann::json TerminologySetup::analyze() const {
    std::vector<TreeNodeStats> stats;
    for (const auto& k: terminology.rootKeys()) {
      traverseTerminology(k, terminology, &stats);
    }
    nlohmann::json nodeStats;
    for (auto s: stats) {
      nodeStats.push_back(nlohmann::json{{"inner_count", s.innerNodeCount}, {"leaf_count", s.leafNodeCount}});
    }

    nlohmann::json dst;
    dst["node_stats"] = nodeStats;
    return dst;
  }

  TerminologyTreeLookup::TerminologyTreeLookup(const TerminologyTreeNode::Ptr tree) {
    _root = initializeNodeData(0, UNDEFINED_INDEX, tree);
    CHECK(_root == 0);
  }

  int TerminologyTreeLookup::initializeNodeData(
    int depth, int parent, const TerminologyTreeNode::Ptr& tree) {
    int index = _data.size();
    {
      NodeData init;
      init.key = tree->key();
      init.parent = parent;
      init.index = index;
      init.depth = depth;
      _data.push_back(init);
      _node2index[init.key] = index;
    }
    for (const auto& c: tree->children()) {
      int childIndex = initializeNodeData(depth+1, index, c);
      _data[index].children.push_back(childIndex);
    }
    return index;
  }

  const TerminologyTreeLookup::NodeData &TerminologyTreeLookup::get(const SkosKey& k) const {
    auto f = _node2index.find(k);
    CHECK(f != _node2index.end());
    return _data[f->second];
  }

  const TerminologyTreeLookup::NodeData &TerminologyTreeLookup::get(int i) const {
    CHECK(0 <= i);
    CHECK(i < _data.size());
    return _data[i];
  }

  TerminologyTreeLookup::PathSummary TerminologyTreeLookup::summarizePath(int src0, int dst0) const {
    int src = src0;
    int dst = dst0;
        
    PathSummary result;
    while (true) {
      CHECK(src != UNDEFINED_INDEX);
      CHECK(dst != UNDEFINED_INDEX);
      if (src == dst) {
        result.minimumCommonNode = src;
        return result;
      }
      const auto& srcData = _data[src];
      const auto& dstData = _data[dst];

      bool ascendSrc = srcData.depth >= dstData.depth;
      bool descendDst = dstData.depth >= srcData.depth;

      if (ascendSrc) {
        src = srcData.parent;
        result.ascentSteps++;
      }
      if (descendDst) {
        dst = dstData.parent;
        result.descentSteps++;
      }
    }
    return result;
  }

  void TerminologySetup::loadFromJson(const nlohmann::json& src) {
    terminology = SkosTerminology::loadFromJsonld(src["filename"]);
    auto tree = buildTree();
    treeLookup = std::make_shared<TerminologyTreeLookup>(tree);
  }


}

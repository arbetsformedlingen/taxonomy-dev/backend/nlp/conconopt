#ifndef __TREE_STATE_OPTIMIZER_HPP__
#define __TREE_STATE_OPTIMIZER_HPP__

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <iosfwd>
#include "utils.hpp"
#include <nlohmann/json.hpp>
#include "TreeStateOptimizationProblem.hpp"

namespace ccopt {
  
  namespace TreeStateOptimizerImpl {
    template <typename Cost>
    void checkTraverseProblem(ITreeStateOptimizationProblem<Cost>* problem, NodeIndex node, std::unordered_set<NodeIndex>* seenNodes) {
      CHECK(seenNodes->find(node) == seenNodes->end());
      CHECK(0 < problem->getStates(node).size());
      seenNodes->insert(node);
      for (NodeIndex child: problem->getChildren(node)) {
        checkTraverseProblem(problem, child, seenNodes);
      }
    }

    template <typename Cost>
    void checkProblem(ITreeStateOptimizationProblem<Cost>* problem) {
      std::unordered_set<NodeIndex> seenNodes;
      checkTraverseProblem(problem, problem->getRootNode(), &seenNodes);
    }

    template <typename Key, typename Cost>
    struct StateCost {
      Key state = Key(-1);
      Cost cost = Cost(std::numeric_limits<double>::infinity());

      StateCost() {}
      StateCost(Key s, Cost c) : state(s), cost(c) {}

      bool operator<(const StateCost<Key, Cost>& other) const {
        return cost < other.cost;
      }
      typedef StateCost<Key, Cost> This;

      NLOHMANN_DEFINE_TYPE_INTRUSIVE(This, state, cost)
    };

    template <typename Cost>
    struct CandAcc {
      StateCost<int, Cost> bestCost;

      void add(const StateCost<int, Cost>& candidate) {
        if (candidate < bestCost) {
          bestCost = candidate;
        }
      }

      const StateCost<int, Cost>& best() const {
        return bestCost;
      }

      CandAcc<Cost> reset() const {
        return {};
      }

      NLOHMANN_DEFINE_TYPE_INTRUSIVE(CandAcc<Cost>, bestCost);
    };

    template <typename Cost>
    struct SolutionForState {
      SolutionForState() {}
      SolutionForState(StateIndex si) : stateIndex(si) {}

      bool enabled = true;
      StateIndex stateIndex;
      Cost totalCost = Cost(std::numeric_limits<double>::infinity());
      std::vector<StateCost<int, Cost>> costsPerChild;

      void displayInfo(std::ostream* dst) const {
        *dst << "    state=" << stateIndex << " enabled=" << enabled << " totalCost=" << totalCost << "\n";
      }

      bool operator<(const SolutionForState& other) const {
        return totalCost < other.totalCost;
      }

      NLOHMANN_DEFINE_TYPE_INTRUSIVE(SolutionForState<Cost>, stateIndex, totalCost, costsPerChild, enabled)
    };

    template <typename T>
    std::vector<T> vectorFromJson(const nlohmann::json& src) {
      std::vector<T> dst;
      for (const auto& x: src) {
        T y;
        y = x;
        dst.push_back(y);
      }
      return dst;
    }

    template <typename T>
    nlohmann::json vectorToJson(const std::vector<T>& src) {
      nlohmann::json dst;
      for (const auto& x: src) {
        nlohmann::json y;
        y = x;
        dst.push_back(y);
      }
      return dst;
    }

    template <typename Cost>
    struct SolutionForNode {
      int parentIndex = -1;
      NodeIndex nodeIndex;
      std::vector<SolutionForState<Cost>> solutionPerState;
      std::vector<int> internalChildInds;
      std::unordered_map<StateIndex, int> stateMap;
      bool debug = false;


      std::vector<StateIndex> getStateInds() const {
        std::vector<StateIndex> dst;
        for (const auto& s: solutionPerState) {
          dst.push_back(s.stateIndex);
        }
        return dst;
      }

      std::unordered_set<StateIndex> getEnabledStateInds() const {
        std::unordered_set<StateIndex> dst;
        for (const auto& s: solutionPerState) {
          if (s.enabled) {
            dst.insert(s.stateIndex);
          }
        }
        return dst;
      }

      int countEnabled() const {
        int counter = 0;
        for (const auto& s: solutionPerState) {
          if (s.enabled) {
            counter++;
          }
        }
        return counter;
      }

      bool allEnabled() const {
        return countEnabled() == solutionPerState.size();
      }

      void setDebug(bool d) {
        debug = true;
      }

      void displayInfo(std::ostream* dst) {
        *dst << "  " << nodeIndex << " with states\n";
        for (const auto& ss: solutionPerState) {
          ss.displayInfo(dst);
        }
        *dst << "  parent at " << parentIndex << std::endl;
      }

      void checkConsistency() {
        CHECK(stateMap.size() == solutionPerState.size());
        for (size_t i = 0; i < solutionPerState.size(); i++) {
          CHECK(stateMap.at(solutionPerState[i].stateIndex) == i);
        }
      }

      SolutionForNode() {}
      SolutionForNode(NodeIndex i, const std::vector<StateIndex>& states, const std::vector<int>& ci) : internalChildInds(ci) {
        nodeIndex = i;
        for (StateIndex state: states) {
          solutionPerState.push_back(SolutionForState<Cost>(state));
        }
        rebuild();
        //checkConsistency();
      }

      const SolutionForState<Cost>& getSolutionForState(StateIndex stateIndex) const {
        for (const auto& sol: solutionPerState) {
          if (sol.stateIndex == stateIndex) {
            return sol;
          }
        }
        CRASH("No state with index ");
      }

      template <typename Acc>
      const SolutionForState<Cost>& bestState(const Acc& accProto) const {
        auto acc = accProto.reset();
        for (size_t i = 0; i < solutionPerState.size(); i++) {
          auto& ss = solutionPerState[i];
          if (ss.enabled) {
            StateCost<int, Cost> cand;
            cand.state = i;
            cand.cost = ss.totalCost;
            acc.add(cand);
          }
        }
        return solutionPerState[acc.best().state];
      }

      template <typename StateCostVisitor>
      void visitAll(
          NodeIndex parentNodeIndex,
          StateIndex parentStateIndex,
          ITreeStateOptimizationProblem<Cost>* problem, StateCostVisitor* visitor) const {
        for (size_t i = 0; i < solutionPerState.size(); i++) {
          const auto& childState = solutionPerState[i];
          if (childState.enabled) {
            Cost transitionCost = problem->getTransitionCost(
              nodeIndex, parentNodeIndex, childState.stateIndex, parentStateIndex);
            ccopt::TreeStateOptimizerImpl::StateCost<int, Cost> cand;
            cand.state = i;
            cand.cost = transitionCost + childState.totalCost;
            visitor->add(cand);
          }
        }
      }

      void recompute(ITreeStateOptimizationProblem<Cost>* problem, std::vector<SolutionForNode<Cost>>* postorder) {
        // For every state of that node, find the best solution for that state
        //checkConsistency();
        for (auto& statesol: solutionPerState) {
          if (statesol.enabled) {
            statesol.costsPerChild.resize(0);
            Cost totalCost = problem->getStateCost(nodeIndex, statesol.stateIndex);

            // Given a state of the node, consider every child.
            for (const auto& childIndex: internalChildInds) {
              CandAcc<Cost> candAcc;
              auto& childSol = (*postorder)[childIndex];
              childSol.visitAll(nodeIndex, statesol.stateIndex, problem, &candAcc);
              totalCost = totalCost + candAcc.best().cost;
              statesol.costsPerChild.push_back(candAcc.best());
            }
            CHECK(statesol.costsPerChild.size() == internalChildInds.size());
            statesol.totalCost = totalCost;
          }
        }
        //checkConsistency();
      }

      void rebuild() {
        for (size_t i = 0; i < solutionPerState.size(); i++) {
          auto state = solutionPerState[i].stateIndex;
          CHECK(stateMap.count(state) == 0);
          stateMap[state] = i;
        }
      }

      //NLOHMANN_DEFINE_TYPE_INTRUSIVE(SolutionForNode<Cost>, nodeIndex, solutionPerState, childInds)
    };

    template <typename Cost>
    void to_json(nlohmann::json& j, const SolutionForNode<Cost>& p) {
      j = nlohmann::json{
        {"nodeIndex", p.nodeIndex},
        {"parentIndex", p.parentIndex},
        {"solutionPerState", p.solutionPerState},
        {"internalChildInds", p.internalChildInds},
      };
    }

    template <typename Cost>
    void from_json(const nlohmann::json& j, SolutionForNode<Cost>& p) {
      j.at("nodeIndex").get_to(p.nodeIndex);
      j.at("parentIndex").get_to(p.parentIndex);
      p.solutionPerState = vectorFromJson<SolutionForState<Cost>>(j.at("solutionPerState"));
      j.at("internalChildInds").get_to(p.internalChildInds);
      p.rebuild();
    }




    template <typename Cost>
    void initializePostorder(
        ITreeStateOptimizationProblem<Cost>* problem, NodeIndex node,
        std::vector<SolutionForNode<Cost>>* dst,
        std::unordered_map<NodeIndex, int>* nodeArrayIndexMap) {

      // First: visit all children
      for (NodeIndex child: problem->getChildren(node)) {
        initializePostorder(problem, child, dst, nodeArrayIndexMap);
      }

      // Second: Visit this node and initialize data
      int currentIndex = dst->size();
      (*nodeArrayIndexMap)[node] = currentIndex;
      std::vector<int> childInds;
      for (NodeIndex child: problem->getChildren(node)) {
        auto f = nodeArrayIndexMap->find(child);
        CHECK(f != nodeArrayIndexMap->end());
        int childIndex = f->second;
        childInds.push_back(childIndex);
        (*dst)[childIndex].parentIndex = currentIndex;
      }
      dst->push_back(SolutionForNode<Cost>(node, problem->getStates(node), childInds));
    }

    template <typename Cost>
    void traverseSolution(
        const SolutionForNode<Cost>& node,
        const SolutionForState<Cost>& bestState,
        const std::vector<SolutionForNode<Cost>>& postorder,
        std::unordered_map<NodeIndex, StateIndex>* node2stateMap) {
      (*node2stateMap)[node.nodeIndex] = bestState.stateIndex;
      int childCount = node.internalChildInds.size();
      CHECK(childCount == bestState.costsPerChild.size());
      for (int i = 0; i < childCount; i++) {
        const auto& nodeToVisit = postorder[node.internalChildInds[i]];
        traverseSolution(
            nodeToVisit,
            nodeToVisit.solutionPerState[bestState.costsPerChild[i].state],
            postorder, node2stateMap);
      }
    }

    template <typename Cost, typename Acc>
    void traverseSolutionWithAcc(ITreeStateOptimizationProblem<Cost>* problem,
        const SolutionForNode<Cost>& node,
        const SolutionForState<Cost>& bestState,
        const std::vector<SolutionForNode<Cost>>& postorder,
        std::unordered_map<NodeIndex, StateIndex>* node2stateMap,
        const Acc& accProto) {
      (*node2stateMap)[node.nodeIndex] = bestState.stateIndex;
      int childCount = node.internalChildInds.size();
      CHECK(childCount == bestState.costsPerChild.size());
      for (int i = 0; i < childCount; i++) {
        const auto& childNode = postorder[node.internalChildInds[i]];
        Acc acc = accProto.reset();
        childNode.visitAll(node.nodeIndex, bestState.stateIndex, problem, &acc);
        auto bestChildState = acc.best();
        traverseSolutionWithAcc(
            problem,
            childNode, childNode.solutionPerState[bestChildState.state],
            postorder, node2stateMap, accProto);
      }
    }
  }

  struct NodeStateEnabledState {
    NodeIndex nodeIndex;
    StateIndex stateIndex;
    bool value = true;

    NodeStateEnabledState() {}
    NodeStateEnabledState(int ni, int si, bool v)
      : nodeIndex(ni), stateIndex(si), value(v) {}
  };

  template <typename Cost>
  class TreeStateOptimizationAccumulation {
  public:
    TreeStateOptimizationAccumulation() {}

    TreeStateOptimizationAccumulation(
        const std::unordered_map<NodeIndex, int>& nodeArrayIndexMap,
        const std::vector<ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost>>& postorder)
          : _nodeArrayIndexMap(nodeArrayIndexMap), _postorder(postorder) {}

    TreeStateOptimizationAccumulation(
        ITreeStateOptimizationProblem<Cost>* problem,
        const TreeStateOptimizerSettings& settings) {
      using namespace ccopt::TreeStateOptimizerImpl;
      checkProblem(problem);
      ccopt::TreeStateOptimizerImpl::initializePostorder(problem, problem->getRootNode(), &_postorder, &_nodeArrayIndexMap);

      // Accumulate all the costs bottom up.
      int counter = 0;

      // Loop over every node in the tree
      for (auto& nodesol: _postorder) {
        problem->reportAccumulationProgress(counter, _postorder.size());
        nodesol.recompute(problem, &_postorder);
        counter += 1;
      }
      problem->reportAccumulationProgress(_postorder.size(), _postorder.size());
    }

    TreeStateOptimizationSolution<Cost> solve() const {
      typedef ccopt::TreeStateOptimizerImpl::CandAcc<Cost> Acc;
      // Unwind the solution from the root
      const auto& root = _postorder.back();
      const auto& bestRootState = root.template bestState<Acc>(Acc());
      TreeStateOptimizationSolution<Cost> result;
      result.cost = bestRootState.totalCost;
      traverseSolution(root, bestRootState, _postorder, &result.node2stateMap);
      return result;
    }

    template <typename Acc>
    TreeStateOptimizationSolution<Cost> solveWithAcc(
        ITreeStateOptimizationProblem<Cost>* problem,
        const Acc& accProto) const {
      const auto& root = _postorder.back();
      const auto& bestRootState = root.template bestState<Acc>(accProto);
      TreeStateOptimizationSolution<Cost> result;
      traverseSolutionWithAcc(problem, root, bestRootState, _postorder, &result.node2stateMap, accProto);
      result.cost = result.reevaluate(problem).totalCost();
      return result;
    }

    const std::unordered_map<NodeIndex, int>& nodeArrayIndexMap() const {
      return _nodeArrayIndexMap;
    }

    const ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost>& solutionForNode(NodeIndex i) const {
      return _postorder[_nodeArrayIndexMap.at(i)];
    }

    const std::vector<ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost>>& postorder() const {
      return _postorder;
    }

    void changeEnabledStates(ITreeStateOptimizationProblem<Cost>* problem, const std::vector<NodeStateEnabledState>& changes) {
      std::unordered_set<int> nodesToRecompute;
      for (const auto& c: changes) {
        int nodeIndex = _nodeArrayIndexMap[c.nodeIndex];
        auto& nodeSol = _postorder[nodeIndex];
        int stateIndex = nodeSol.stateMap.at(c.stateIndex);
        auto& stateSol = nodeSol.solutionPerState[stateIndex];
        CHECK(stateSol.stateIndex == c.stateIndex);
        if (stateSol.enabled != c.value) {
          stateSol.enabled = c.value;
          int index = nodeIndex;
          while (nodesToRecompute.count(index) == 0 && index != -1) {
            nodesToRecompute.insert(index);
            index = _postorder[index].parentIndex;
          }
        }
      }
      std::vector<int> nodeInds(nodesToRecompute.begin(), nodesToRecompute.end());
      std::sort(nodeInds.begin(), nodeInds.end());
      for (auto i: nodeInds) {
        _postorder[i].recompute(problem, &_postorder);
      }
    }

    void displayInfo(std::ostream* dst) {
      *dst << "*** Accumulation info for " << _postorder.size() << " nodes\n";
      for (size_t i = 0; i < _postorder.size(); i++) {
        *dst << "_postorder[" << i << "]:\n";
        _postorder[i].displayInfo(dst);
      }
    }

    size_t size() const {
      return _postorder.size();
    }

    bool empty() const {
      return size() == 0;
    }

    //NLOHMANN_DEFINE_TYPE_INTRUSIVE(TreeStateOptimizationAccumulation<Cost>, _childMap, _postorder)
  private:
    std::unordered_map<NodeIndex, int> _nodeArrayIndexMap;
    std::vector<ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost>> _postorder;
  };


  template <typename Cost>
  void to_json(nlohmann::json& j, const TreeStateOptimizationAccumulation<Cost>& p) {
    j = nlohmann::json{{"nodeArrayIndexMap", p.nodeArrayIndexMap()}, {"postorder", p.postorder()}};
  }

  template <typename Cost>
  void from_json(const nlohmann::json& j, TreeStateOptimizationAccumulation<Cost>& p) {
    std::unordered_map<NodeIndex, int> nodeArrayIndexMap;
    std::vector<ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost>> postorder;
    j.at("nodeArrayIndexMap").get_to(nodeArrayIndexMap);
    for (const auto& x: j.at("postorder")) {
      ccopt::TreeStateOptimizerImpl::SolutionForNode<Cost> y;
      y = x;
      postorder.push_back(y);
    }
    p = TreeStateOptimizationAccumulation<Cost>(nodeArrayIndexMap, postorder);
  }


  template <typename Cost>
  TreeStateOptimizationSolution<Cost> optimize(
      ITreeStateOptimizationProblem<Cost>* problem,
      const TreeStateOptimizerSettings& settings = TreeStateOptimizerSettings()) {
    return TreeStateOptimizationAccumulation<Cost>(problem, settings).solve();
  }

  template <typename Cost>
  std::ostream& operator<<(std::ostream& out, const TreeStateOptimizationSolution<Cost>& sol) {
    out << "TreeStateOptimizationSolution with cost " << sol.cost << ":";
    for (const auto& kv: sol.node2stateMap) {
      out << "\n  * " << kv.first << " --> " << kv.second;
    }
    return out;
  }

  void testTreeStateOptimizer();
  
}



#endif

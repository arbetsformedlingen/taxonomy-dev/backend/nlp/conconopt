/*
 * TreeStateOptimizationProblem.hpp
 *
 *  Created on: Oct 25, 2022
 *      Author: jonas
 */

#ifndef TREESTATEOPTIMIZATIONPROBLEM_HPP_
#define TREESTATEOPTIMIZATIONPROBLEM_HPP_

#include <vector>
#include <iosfwd>

template <typename This>
class Index {
public:
  static const int UNDEFINED_INDEX = -1;

  int value = UNDEFINED_INDEX;

  Index() : value(-1) {}
  Index(int i) : value(i) {}

  bool isDefined() const {
    return value != UNDEFINED_INDEX;
  }

  bool operator<(const This& other) const {
    return value < other.value;
  }

  bool operator>(const This& other) const {
    return value > other.value;
  }

  bool operator==(const This& other) const {
    return value == other.value;
  }

  bool operator<=(const This& other) const {
    return value <= other.value;
  }

  bool operator>=(const This& other) const {
    return value >= other.value;
  }

  bool operator!=(const This& other) const {
    return value != other.value;
  }
};

template<typename T>
struct std::hash<Index<T>> {
  std::size_t operator()(const Index<T>& x) const noexcept {
    std::hash<int> sub;
    return sub(x.value);
  }
};


class NodeIndex : public Index<NodeIndex> {
public:
  NodeIndex() {}
  explicit NodeIndex(int i) : Index<NodeIndex>(i) {}
};
template <> struct std::hash<NodeIndex> : public std::hash<Index<NodeIndex>> {};

class StateIndex : public Index<StateIndex> {
public:
  StateIndex() {}
  explicit StateIndex(int i) : Index<StateIndex>(i) {}
};
template <> struct std::hash<StateIndex> : public std::hash<Index<StateIndex>> {};

std::ostream& operator<<(std::ostream& s, NodeIndex i);
std::ostream& operator<<(std::ostream& s, StateIndex i);

std::vector<NodeIndex> nodeInds(const std::vector<int>& src);
std::vector<StateIndex> stateInds(const std::vector<int>& src);

template <typename T>
void to_json(nlohmann::json& j, const Index<T>& p) {
  j = nlohmann::json(p.value);
}

template <typename T>
void from_json(const nlohmann::json& j, Index<T>& p) {
  p.value = j;
}



namespace ccopt {

template <typename Cost>
class ITreeStateOptimizationProblem {
public:
  virtual NodeIndex getRootNode() const = 0;
  virtual const std::vector<NodeIndex>& getChildren(NodeIndex nodeIndex) const = 0;
  virtual const std::vector<StateIndex>& getStates(NodeIndex nodeIndex) const = 0;
  virtual Cost getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const = 0;
  virtual Cost getTransitionCost(
      NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
      StateIndex childState, StateIndex parentState) const = 0;
  virtual ~ITreeStateOptimizationProblem() {}
  virtual void reportAccumulationProgress(int processedCount, int totalCount) {};
};

template <typename Cost>
struct TreeStateOptimizationSolution {
  Cost cost = Cost(std::numeric_limits<double>::infinity());
  std::unordered_map<NodeIndex, StateIndex> node2stateMap;

  TreeStateOptimizationSolution() {}
  TreeStateOptimizationSolution(const std::unordered_map<NodeIndex, StateIndex>& n2s, ITreeStateOptimizationProblem<Cost>* problem)
    : node2stateMap(n2s) {
    cost = reevaluate(problem).totalCost();
  }

  TreeStateOptimizationSolution(const std::unordered_map<NodeIndex, StateIndex>& n2s, double c)
    : node2stateMap(n2s), cost(c) {}

  struct StateTransitionCostPair {
    Cost stateCost = Cost(0.0);
    Cost transitionCost = Cost(0.0);

    StateTransitionCostPair() {}

    StateTransitionCostPair(const Cost &sc, const Cost& tc)
    : stateCost(sc), transitionCost(tc) {}

    Cost totalCost() const {
      return stateCost + transitionCost;
    }

    StateTransitionCostPair operator+(const StateTransitionCostPair& other) const {
      return {stateCost + other.stateCost, transitionCost + other.transitionCost};
    }
  };

  StateTransitionCostPair reevaluate(ITreeStateOptimizationProblem<Cost>* problem) const {
    return reevaluateSubtree(problem, problem->getRootNode());
  }

  bool operator<(const TreeStateOptimizationSolution<Cost>& other) const {
    return cost < other.cost;
  }

  StateTransitionCostPair reevaluateSubtree(ITreeStateOptimizationProblem<Cost>* problem, NodeIndex node) const {
    StateTransitionCostPair costPair;
    StateIndex state = node2stateMap.at(node);
    costPair.stateCost = problem->getStateCost(node, state);
    for (NodeIndex child: problem->getChildren(node)) {
      StateIndex childState = node2stateMap.at(child);
      costPair = costPair + reevaluateSubtree(problem, child);
      costPair.transitionCost = costPair.transitionCost + problem->getTransitionCost(child, node, childState, state);
    }
    return costPair;
  }
};


struct TreeStateOptimizerSettings {

};


}



#endif /* TREESTATEOPTIMIZATIONPROBLEM_HPP_ */

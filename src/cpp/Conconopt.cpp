#include <iostream>
#include "Conconopt.hpp"
#include <nlohmann/json.hpp>
#include <fstream>
#include <iostream>
#include "TreeStateOptimizer.hpp"
#include "TreeStateOptimizerParameterizedNode.hpp"
#include <random>
#include <chrono>
#include <map>
#include <csv.hpp>
#include <filesystem>
#include <optional>
#include "TextUI.hpp"

namespace ccopt {

DiscreteWeightMapping::DiscreteWeightMapping(int base, int resolution) : _base(base), _resolution(resolution), _lnBase(log(base)) {}

DiscreteWeightMapping DiscreteWeightMapping::centMapping() {
  return {10, 100};
}

int DiscreteWeightMapping::map(double x) const {
  return int(round(_resolution*log(x)/_lnBase));
}

double DiscreteWeightMapping::unmap(int y) const {
  return std::pow(double(_base), (double(y)/_resolution));
}

std::string DiscreteWeightMapping::encodeValue(double v) const {
  int i = map(v);
  std::stringstream ss;
  if (i < 0) {
    ss << "neg";
  } else if (i == 0) {
    ss << "zer";
  } else {
    ss << "pos";
  }
  ss << std::abs(i);
  return ss.str();
}

double DiscreteWeightMapping::decodeString(const std::string& x) const {
  CHECK(4 <= x.size());
  auto signCode = x.substr(0, 3);
  int sign = 0;
  if (signCode == "pos") {
    sign = 1;
  } else if (signCode == "neg") {
    sign = -1;
  }
  auto valuePart = x.substr(3);
  std::stringstream ss;
  ss << valuePart;
  int v;
  ss >> v;
  return unmap(sign*v);
}

std::shared_ptr<ConconoptProblem> ParameterSweep::problemForIndex(ConconoptSetup* setup, int i) {
  auto originalTerms = setup->realizeTerms();
  LOG("Tweak problem " << (i+1) << "/" << _count << " with weight " << valueForIndex(i));
  auto terms = tweakObjectiveFunctionTermsForIndex(originalTerms, i);
  for (const auto& kv: terms) {
    LOG("  * " << kv.first << ": " << kv.second.weight);
  }
  return setup->initializeOptimizationProblem(terms);
}

void ParameterSweep::perform(ConconoptSetup* setup) {
  for (int i = 0; i < _count; i++) {
    auto problem = problemForIndex(setup, i);
    auto acc = setup->accumulate(problem);
    LOG("Accumulated costs");

    std::string filename = filenameForIndex(i);
    nlohmann::json data;
    data = acc;
    std::ofstream file(filename);
    file << data;
    LOG("Saved costs to " << filename);
  }
}

ParameterSweep::ParameterSweep(const nlohmann::json& ps) {
  _weightMapping = DiscreteWeightMapping::centMapping();
  _termKey = ps["term"];
  _count = ps["count"];
  _minValue = ps["min_value"];
  _maxValue = ps["max_value"];
  _outputPrefix = ps["output_prefix"];
}

double ParameterSweep::valueForIndex(int i) const {
  if (_count < 2) {
    return _minValue;
  }
  double y0 = log(_minValue);
  double y1 = log(_maxValue);
  double x0 = 0;
  double x1 = _count - 1;
  double k = (y1 - y0)/(x1 - x0);
  double m = y0 - k*x0;
  double value = exp(k*i + m);
  return _weightMapping.normalize(value);
}

std::string ParameterSweep::filenameForIndex(int i) const {
  double v = valueForIndex(i);
  return _outputPrefix + "_" + _weightMapping.encodeValue(v) + ".json";
}

std::map<std::string, WeightedConconoptCost> ParameterSweep::tweakObjectiveFunctionTermsForIndex(
    const std::map<std::string, WeightedConconoptCost>& terms,
    int i) const {
  auto dst = terms;
  auto f = dst.find(_termKey);
  CHECK(f != dst.end());
  f->second.weight = valueForIndex(i);
  return dst;
}


  namespace {




    
    class ParentChildRegCost : public IConconoptCost {
    public:
      ParentChildRegCost(const TerminologyTreeLookup::Ptr& dstLookup) : _dstLookup(dstLookup) {}
      
      double getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const override {
        return 0.0;
      }
      
      double getTransitionCost(
        NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
        StateIndex childState, StateIndex parentState) const override {
        return _dstLookup->summarizePath(childState.value, parentState.value).hasIndirectParent()? 0.0 : 1.0;
      }

      std::string toString() const override {
        return "ParentChildRegCost";
      }
    private:
      TerminologyTreeLookup::Ptr _dstLookup;
    };

    class AccumulateTask : public IConconoptTask {
    public:
      std::string taskName() const override {return "AccumulateTask";}

      void perform(ConconoptSetup* setup) override {

        auto problem = setup->initializeOptimizationProblem();

        LOG("Accumulating costs...");

        auto acc = setup->accumulate(problem);

        LOG("Accumulated costs");

        nlohmann::json data;
        data = acc;

        std::ofstream file(setup->accumulatedCostsFilename);
        file << data;
        LOG("Saved costs to " << setup->accumulatedCostsFilename);
      }
    };

    class AnalyzeProblemTask : public IConconoptTask {
    public:
      AnalyzeProblemTask(const std::string& outputFilename) : _outputFilename(outputFilename) {}

      std::string taskName() const override {return "AnalyzeProblemTask";}

      void perform(ConconoptSetup* setup) override {
        nlohmann::json objTermAnalyses;

        for (auto& kv: setup->objectiveFunctionTerms) {
          LOG("Analyzing term...")
          auto term = kv.second.get();

          auto analysis = term.cost->analyze();
          if (!analysis.is_null()) {
            objTermAnalyses.push_back(analysis);
          }
        }

        LOG("Done analyses");

        nlohmann::json dst;
        dst["src"] = setup->src.analyze();
        dst["dst"] = setup->dst.analyze();
        dst["objective_function_terms"] = objTermAnalyses;

        std::ofstream file(_outputFilename);
        file << dst;
      }
    private:
      std::string _outputFilename;
    };

    class GaussianCandAcc {
    public:
      typedef ccopt::TreeStateOptimizerImpl::StateCost<int, GaussianCost> Candidate;

      GaussianCandAcc(double sigma, std::default_random_engine* rng) : _sigma(sigma), _rng(rng) {}

      GaussianCandAcc reset() const {
        return GaussianCandAcc(_sigma, _rng);
      }

      void add(const Candidate& c) {
        Candidate cand;
        cand.cost = GaussianCost(std::normal_distribution<double>(
            c.cost.mu(),
            _sigma*c.cost.sigma())(*_rng), c.cost.sigmaSquared());
        cand.state = c.state;
        _best = std::min(_best, cand);
      }

      const Candidate& best() const {
        return _best;
      }
    private:
      Candidate _best;
      double _sigma;
      std::default_random_engine* _rng;
    };

    class SampleSolutionsTask : public IConconoptTask {
    public:
      std::string taskName() const override {return "SampleSolutionsTask";}

      SampleSolutionsTask(int sampleCount, double sigma, const std::string& outputFilename) :
        _sampleCount(sampleCount), _sigma(sigma), _outputFilename(outputFilename) {}

      void perform(ConconoptSetup* setup) override;
    private:
      int _sampleCount;
      double _sigma;
      std::string _outputFilename;
    };

    void sampleSolutions(
        ConconoptSetup* setup,
        ITreeStateOptimizationProblem<GaussianCost>* problem,
        const TreeStateOptimizationAccumulation<GaussianCost>& acc, int sampleCount, double sigma,
        std::default_random_engine* rng, const std::function<void(TreeStateOptimizationSolution<GaussianCost>)>& callback) {
      GaussianCandAcc candAcc(sigma, rng);
      for (int i = 0; i < sampleCount; i++) {
        LOG("Sample " << i+1 << " of " << sampleCount);
        auto solution = acc.solveWithAcc(problem, candAcc);
        callback(solution);
      }
      LOG("Done samples.");
    }

    TreeStateOptimizationAccumulation<GaussianCost> loadAccumulator(const std::string& filename) {
      if (!std::filesystem::exists(filename)) {
        return {};
      }
      std::ifstream file(filename);
      auto data = nlohmann::json::parse(file);
      TreeStateOptimizationAccumulation<GaussianCost> acc;
      acc = data;
      return acc;
    }

    void SampleSolutionsTask::perform(ConconoptSetup* setup) {
      auto acc = loadAccumulator(setup->accumulatedCostsFilename);
      auto problem = setup->initializeOptimizationProblem();
      LOG("Sample " << _sampleCount << " solutions.");
      std::vector<ConceptMapping> solutions;
      sampleSolutions(setup, problem.get(), acc, _sampleCount, _sigma, &(setup->rng),
          [&](const TreeStateOptimizationSolution<GaussianCost>& solution) {
        solutions.push_back(setup->conceptMappingFromSolution(solution, {}));
      });
      LOG("Compute optimum")
      auto optimum = setup->conceptMappingFromSolution(acc.solve(), {"optimum"});
      LOG("Computed.");
      solutions.push_back(optimum);
      auto total = ConceptMapping::merge(solutions);

      setup->writeConceptMappingCSV(_outputFilename, total);

      LOG("Done sampling, merged size is " << total.srcDstPairs.size() << ", optimum is " << optimum.srcDstPairs.size());
    }

    namespace {
      int countDifferences(
          const TreeStateOptimizationSolution<GaussianCost>& a,
          const TreeStateOptimizationSolution<GaussianCost>& b) {
        int diffs = 0;
        CHECK(a.node2stateMap.size() == b.node2stateMap.size());
        for (const auto& kv: a.node2stateMap) {
          auto f = b.node2stateMap.find(kv.first);
          CHECK(f != b.node2stateMap.end());
          if (f->second != kv.second) {
            diffs++;
          }
        }
        return diffs;
      }
    }

    struct ConditionNumberData {
      double weight = 0.0;
      double sigma = 0.0;
      int sampleCount = 0;
      int maxDiff = 0;

      NLOHMANN_DEFINE_TYPE_INTRUSIVE(ConditionNumberData, weight, maxDiff);
    };

    class SweepConditionNumberTask : public IConconoptTask {
    public:
      SweepConditionNumberTask(int sampleCount, double sigma, const std::shared_ptr<ParameterSweep>& sweep, const std::string& outputFilename) :
        _sampleCount(sampleCount), _sigma(sigma), _sweep(sweep), _outputFilename(outputFilename) {}

      std::string taskName() const {
        return "SweepConditionNumberTask";
      }

      void perform(ConconoptSetup* setup) {
        LOG("Perform condition number sweep");
        std::vector<ConditionNumberData> result;
        for (int i = 0; i < _sweep->count(); i++) {
          LOG("   Iteration " << (i+1) << " of " << _sweep->count());
          auto acc = loadAccumulator(_sweep->filenameForIndex(i));
          auto problem = _sweep->problemForIndex(setup, i);
          int maxDiff = 0;
          auto opt = acc.solve();
          sampleSolutions(setup, problem.get(), acc, _sampleCount, _sigma, &(setup->rng),
              [&](const TreeStateOptimizationSolution<GaussianCost>& sol) {
            maxDiff = std::max(maxDiff, countDifferences(opt, sol));
          });
          ConditionNumberData cnd;
          cnd.maxDiff = maxDiff;
          cnd.weight = _sweep->valueForIndex(i);
          cnd.sigma = _sigma;
          cnd.sampleCount = _sampleCount;
          LOG("CONDITION NUMBER " << maxDiff << " for weight " << cnd.weight);
          result.push_back(cnd);
        }
        nlohmann::json jdata;
        jdata = result;
        std::ofstream file(_outputFilename);
        file << jdata;
      }
    private:
      int _sampleCount;
      double _sigma;
      std::shared_ptr<ParameterSweep> _sweep;
      std::string _outputFilename;
    };

    namespace {
      struct MappingPerWeight {
        MappingPerWeight() {}
        MappingPerWeight(const SkosKey& src) : srcKey(src) {}

        struct KeyForWeight {
          SkosKey dstKey;
          double weight;
        };

        SkosKey srcKey;
        std::vector<KeyForWeight> mapping;
      };

      class MappingPerWeightOrder {
      public:
        MappingPerWeightOrder(const ConconoptSetup* setup) : _setup(setup) {}

        std::tuple<int, int> makeTuple(const MappingPerWeight& x) const {
          std::set<SkosKey> candidates;
          for (const auto& y: x.mapping) {
            candidates.insert(y.dstKey);
          }
          return {_setup->src.treeLookup->get(x.srcKey).depth, candidates.size()};
        }

        bool operator()(const MappingPerWeight& a, const MappingPerWeight& b) const {
          return makeTuple(a) < makeTuple(b);
        }
      private:
        const ConconoptSetup* _setup;
      };

      struct HeaderDataPair {
        std::string header;
        std::string data;
      };

      std::string conceptLabel(const TerminologySetup& setup, const SkosKey& k, const std::string lang) {
        std::string typeId = k.type()->id();
        if (k.id().empty()) {
          return typeId;
        }
        auto lk = SkosConcept::makeKey(k.id());
        auto concept = setup.terminology.find(lk);
        if (concept == nullptr) {
          LOG("Not found: " << lk.toString());
          return typeId + ":" + lk.id();
        }
        return typeId + ": " + findLabelInLanguage(
            concept->concept.prefLabels(), lang).value();
      }

      std::vector<HeaderDataPair> csvData(ConconoptSetup* setup, const MappingPerWeight& row) {
        std::vector<HeaderDataPair> dst;
        dst.push_back({"Source concept", conceptLabel(setup->src, row.srcKey, setup->lang)});
        for (const auto& x: row.mapping) {
          std::stringstream ss;
          ss << "weight = " << x.weight;
          dst.push_back({ss.str(), conceptLabel(setup->dst, x.dstKey, setup->lang)});
        }
        return dst;
      }

      std::vector<std::string> header(const std::vector<HeaderDataPair>& src) {
        std::vector<std::string> dst;
        for (const auto& x: src) {
          dst.push_back(x.header);
        }
        return dst;
      }

      std::vector<std::string> data(const std::vector<HeaderDataPair>& src) {
        std::vector<std::string> dst;
        for (const auto& x: src) {
          dst.push_back(x.data);
        }
        return dst;
      }
    }



    class SweepComparisonTask : public IConconoptTask {
    public:
      SweepComparisonTask(const std::shared_ptr<ParameterSweep>& psweep, const std::string& outputFilename)
      : _psweep(psweep), _outputFilename(outputFilename) {}

      std::string taskName() const override {
        return "SweepComparisonTask";
      }

      void perform(ConconoptSetup* setup) override {
        std::map<SkosKey, MappingPerWeight> results;
        for (int i = 0; i < _psweep->count(); i++) {
          LOG("Solution " << (i+1) << "/" << _psweep->count());
          auto acc = loadAccumulator(_psweep->filenameForIndex(i));
          auto mapping = setup->conceptMappingFromSolution(acc.solve(), {});
          double weight = _psweep->valueForIndex(i);
          for (const auto& kv: mapping.srcDstPairs) {
            if (results.count(kv.src) == 0) {
              results[kv.src] = MappingPerWeight(kv.src);
            }
            results[kv.src].mapping.push_back({kv.dst, weight});
          }
        }

        LOG("Sort lines");
        std::vector<MappingPerWeight> lines;
        for (const auto& kv: results) {
          lines.push_back(kv.second);
        }

        if (lines.empty()) {
          LOG("Nothing to output");
          return;
        }

        std::sort(lines.begin(), lines.end(), MappingPerWeightOrder(setup));

        LOG("Output CSV");
        std::ofstream file(_outputFilename);
        auto writer = csv::make_csv_writer(file);
        writer << header(csvData(setup, lines[0]));
        for (const auto& line: lines) {
          writer << data(csvData(setup, line));
        }
      }
    private:
      std::shared_ptr<ParameterSweep> _psweep;
      std::string _outputFilename;
    };


    IConconoptTask::Ptr parseAccumulateTask(const nlohmann::json& data) {
      return std::make_shared<AccumulateTask>();
    }

    IConconoptTask::Ptr analyzeProblemTask(const nlohmann::json& data) {
      std::string filename = data["output_filename"];
      return std::make_shared<AnalyzeProblemTask>(filename);
    }

    IConconoptTask::Ptr sampleSolutionsTask(ConconoptSetup* setup, const nlohmann::json& data) {
      std::string filename = data["output_filename"];
      return std::make_shared<SampleSolutionsTask>(setup->sampleCount, setup->sigma, filename);
    }

    IConconoptTask::Ptr parameterSweepTask(const nlohmann::json& ps) {
      return std::make_shared<ParameterSweep>(ps);
    }

    IConconoptTask::Ptr sweepConditionNumberTask(ConconoptSetup* setup, const nlohmann::json& data) {
      std::string sweepKey = data["sweep_key"];
      std::string outputFilename = data["output_filename"];
      int sampleCount = data.value("sample_count", setup->sampleCount);
      LOG("Sample count: " << sampleCount);
      return std::make_shared<SweepConditionNumberTask>(
                sampleCount, setup->sigma,
                setup->getTaskOfType<ParameterSweep>(sweepKey), outputFilename);
    }

    IConconoptTask::Ptr sweepComparisonTask(ConconoptSetup* setup, const nlohmann::json& data) {
      std::string sweepKey = data["sweep_key"];
      std::string outputFilename = data["output_filename"];
      return std::make_shared<SweepComparisonTask>(
          setup->getTaskOfType<ParameterSweep>(sweepKey), outputFilename);
    }

    std::string toLower(const std::string& x) {
      std::string dst;
      for (char c: x) {
        dst += std::tolower(c);
      }
      return dst;
    }

    class Menu {
    public:
      std::string generate() {
        for (char c = 'a'; c <= 'z'; c++) {
          std::string s;
          s += c;
          if (_used.count(s) == 0) {
            return use(s);
          }
        }
        CRASH("Nothing");
      }

      bool isValid(const std::string& s) const {
        return s != "";
      }

      std::string useOrGenerate(const std::string& s) {
        if (isValid(s) && _used.count(s) == 0) {
          return use(s);
        } else {
          return generate();
        }
      }

      std::string use(const std::string& s0) {
        auto s = toLower(s0);
        CHECK(_used.count(s) == 0);
        _used.insert(s);
        return s;
      }
    private:
      int _index;
      std::set<std::string> _used;
    };

    template <typename T>
    class BasicInputMatcher {
    public:
      BasicInputMatcher(Menu* menu, TextRenderer* r) : _r(r), _menu(menu) {}

      void bind(const std::string& input, const T& value, const std::string& message) {
        _r->startNewLine() << bindSub(input, value, message);
      }

      void delayedBind(const std::string& input, const T& value, const std::string& message) {
        _last.push_back(bindSub(input, value, message));
      }

      std::optional<T> match(const std::string& input) {
        auto f = _bindings.find(toLower(input));
        if (f == _bindings.end()) {
          return {};
        }
        return f->second;
      }

      BasicInputMatcher(const BasicInputMatcher& other) = delete;
      BasicInputMatcher& operator=(const BasicInputMatcher& other) = delete;

      void finalize() {
        for (const auto& s: _last) {
          _r->startNewLine() << s;
        }
        _last = {};
      }
    private:
      std::string bindSub(const std::string& input, const T& value, const std::string& message) {
        std::string bound = _menu->useOrGenerate(input);
        _bindings[bound] = value;
        return  "(" + bound + ") " + message;
      }

      TextRenderer* _r;
      Menu* _menu;
      std::map<std::string, T> _bindings;
      std::vector<std::string> _last;
    };


    enum class TerminologyKey {
      Src, Dst
    };

    class Editor {
    public:
      struct NodeKey {
        TerminologyKey terminology;
        SkosKey key;

        std::pair<TerminologyKey, SkosKey> pair() const {
          return {terminology, key};
        }

        bool operator==(const NodeKey& other) const {
          return pair() == other.pair();
        }

        bool operator!=(const NodeKey& other) const {
          return pair() != other.pair();
        }

        bool operator<(const NodeKey& other) const {
          return pair() < other.pair();
        }

        NodeKey withKey(const SkosKey& k) const {
          return {terminology, k};
        }
      };

      Editor(ConconoptSetup* setup, const TreeStateOptimizationAccumulation<GaussianCost>& acc, const std::string& workFilename)
        : _setup(setup), _workFilename(workFilename), _acc(acc), _problem(setup->initializeOptimizationProblem()) {
        _nodeHistory.push_back(keyForIndex(TerminologyKey::Src, setup->src.treeLookup->root()));
      }

      struct MainMenuSettings {
        std::string quitDescription;
        NodeKey nodeToAssign;

        MainMenuSettings() {
          quitDescription = "Quit";
        }
      };

      void mainMenu(const MainMenuSettings& settings = MainMenuSettings());
    private:
      void enableMatch(const NodeKey& src, const NodeKey& dst);

      NodeKey currentNode() const {
        return _nodeHistory.back();
      }

      int _currentPage = 0;
      ConconoptSetup* _setup;
      std::string _workFilename;
      int _childrenPerPage = 10;
      int _candidateCount = 10;
      std::shared_ptr<ConconoptProblem> _problem;
      TreeStateOptimizationAccumulation<GaussianCost> _acc;
      std::string label(const std::vector<LangString>& labels) const;
      std::string label(const NodeKey& k) const;
      void displayConceptInfo(const SkosTerminology::ConceptData* data, TextRenderer* dst);

      struct Candidate {
        NodeKey dstKey;
        bool isConstraint = false;
        double cost;

        bool operator<(const Candidate& other) const {
          return cost < other.cost;
        }
      };

      void edit(TextRenderer* r, const NodeKey& k);
      std::vector<Candidate> getCandidates(const NodeKey& src);

      std::vector<NodeKey> _nodeHistory;

      TerminologySetup& getTerminologySetup(TerminologyKey k) const {
        return k == TerminologyKey::Src? _setup->src : _setup->dst;
      }

      TerminologyTreeLookup::NodeData nodeData(NodeKey k) const {
        return getTerminologySetup(k.terminology).treeLookup->get(k.key);
      }
      struct ChildData {
        int pageCount = -1;
        std::vector<NodeKey> children;
      };

      std::set<NodeKey> getMatches(const NodeKey& src) const;
      void changeEnabledState(const NodeKey& src, const std::vector<NodeKey>& dst, bool enabled);
      void enableAll(const NodeKey& src);
      ChildData childData(const NodeKey& k) const;

      const SkosTerminology::ConceptData* conceptData(const NodeKey& k) const {
        return getTerminologySetup(k.terminology).terminology.find(k.key);
      }

      NodeKey keyForIndex(TerminologyKey tkey, int index) const {
        if (index == TerminologyTreeLookup::UNDEFINED_INDEX) {
          return {tkey, SkosKey()};
        } else {
          return {tkey, getTerminologySetup(tkey).treeLookup->get(index).key};
        }
      }

      bool canGoBack() const {
        return 1 < _nodeHistory.size();
      }

      std::vector<Editor::NodeKey> parents(const Editor::NodeKey& k) const;
      Editor::NodeKey parent(const Editor::NodeKey& k) const;
      void goToNode(const NodeKey& key);
      void goUp();
      void goBack();
    };

    template <typename T>
    class WithBackup {
    public:
      WithBackup(T* p) : _ptr(p), _backup(*p) {}
      ~WithBackup() {
        *_ptr = _backup;
      }
    private:
      T* _ptr;
      T _backup;
    };

    struct ParsedConceptCommand {
      std::string command;
      std::vector<std::string> concepts;

      bool isDefined() const {
        return 0 < command.size();
      }
    };

    ParsedConceptCommand parseConceptCommand(const std::vector<std::string>& commands, const std::string& input) {
      ParsedConceptCommand dst;
      for (auto c: commands) {
        std::string searchString = c + " ";
        auto i = input.find(searchString);
        if (i == 0) {
          dst.command = c;
          for (auto k: input.substr(searchString.size())) {
            dst.concepts.push_back(std::string(1, k));
          }
          return dst;
        }
      }
      return dst;
    }

    void Editor::edit(TextRenderer* r, const NodeKey& srcNode) {
      WithBackup<std::vector<Editor::NodeKey>> backup(&_nodeHistory);
      auto r2 = r->deeper("  * ");
      auto candidates = getCandidates(srcNode);

      auto srcNodeIndex = NodeIndex(nodeData(srcNode).index);
      auto stateInds = _problem->getStates(srcNodeIndex);

      int stateCount = stateInds.size();

      while (true) {
        const auto& sol = _acc.solutionForNode(srcNodeIndex);
        int enabledCount = sol.countEnabled();
        bool hasConstraints = enabledCount != stateCount;

        Menu menu;
        BasicInputMatcher<std::string> otherMatcher(&menu, r);
        BasicInputMatcher<NodeKey> conceptMatcher(&menu, &r2);
        otherMatcher.delayedBind("b", "back", "Back to concept");
        if (hasConstraints) {
          otherMatcher.delayedBind("r", "reset", "Reset (enable all constraints)");
        }
        r->clear();
        r->startNewLine() << "*** Assign matches for " << label(srcNode);
        if (!hasConstraints) {
          r->startNewLine() << "No candidate matches chosen.";
        } else {
          r->startNewLine() << enabledCount << " candidate matches";
        }
        r->startNewLine() << "Matching candidate concepts:\n";

        {
          auto rem = getMatches(srcNode);
          for (const auto& c: candidates) {
            if (rem.count(c.dstKey) == 1) {
              rem.erase(c.dstKey);
            }
            auto candData = nodeData(c.dstKey);
            std::string matchMarker;
            if (hasConstraints) {
              if (sol.getSolutionForState(StateIndex(candData.index)).enabled) {
                matchMarker = "[M] ";
              } else {
                matchMarker = "    ";
              }
            }
            std::stringstream ss;
            ss << matchMarker << label(c.dstKey) << " with cost " << c.cost;
            conceptMatcher.bind("", c.dstKey, ss.str());
          }
          for (auto k: rem) {
            std::string matchMarker = "[M] ";
            conceptMatcher.bind("", k, matchMarker + label(k));
          }
        }


        otherMatcher.finalize();
        r->startNewLine() << "'enable' followed by concept codes to enable matches";
        r->startNewLine() << "'disable' followed by concept codes to disable previously enabled matches";

        while (true) {
          r->startNewLine() << "Choice? ";
          std::string input;
          std::getline(std::cin, input);
          {
            auto c = otherMatcher.match(input);
            if (c) {
              if (c.value() == "back") {
                return;
              } else if (c.value() == "reset") {
                enableAll(srcNode);
                break;
              }
            }
          }{
            auto c = conceptMatcher.match(input);
            if (c) {
              goToNode(c.value());
              MainMenuSettings s;
              s.nodeToAssign = srcNode;
              s.quitDescription = "Back to previous menu";
              mainMenu(s);
              break;
            }
          }{
            auto parsed = parseConceptCommand({"enable", "disable"}, input);
            if (parsed.isDefined()) {
              LOG(parsed.command << " " << parsed.concepts.size() << " matches.");

              bool enabledState = parsed.command == "enable";

              std::vector<NodeKey> ksToModify;
              for (const auto& code: parsed.concepts) {
                auto k = conceptMatcher.match(code);
                if (k) {
                  ksToModify.push_back(k.value());
                }
              }

              changeEnabledState(srcNode, ksToModify, enabledState);

              break;
            }
          }
          r->startNewLine() << "Try again.";
        }
      }
    }

    std::vector<Editor::Candidate> Editor::getCandidates(const NodeKey& src) {
      CHECK(src.terminology == TerminologyKey::Src);
      auto srcNodeIndex = NodeIndex(nodeData(src).index);
      ParameterizeForNode<GaussianCost> param(_problem.get(), &_acc, srcNodeIndex);
      auto states = _problem->getStates(srcNodeIndex);
      std::vector<Candidate> candidates;
      param.enableAll(_problem.get());


      int n = std::min(_candidateCount, int(states.size()));
      LOG("Consider the " << n << " best candidates:");
      for (size_t i = 0; i < n; i++) {
        auto pmin = param.minimizeBottom();
        param.disableState(_problem.get(), pmin.state);

        Candidate c;
        c.dstKey = keyForIndex(TerminologyKey::Dst, pmin.state.value);
        c.isConstraint = false; // TODO
        c.cost = pmin.cost.mu();
        LOG("  * Candidate: " << label(c.dstKey) << " at cost " << c.cost);
        candidates.push_back(c);
      }
      return candidates;
    }



    Editor::ChildData Editor::childData(const NodeKey& nodeKey) const {
      ChildData dst;
      auto inds = nodeData(nodeKey).children;
      int indCount = inds.size();
      dst.pageCount = (indCount - 1)/_childrenPerPage + 1;
      int lowerChild = _currentPage*_childrenPerPage;
      int upperChild = std::min(indCount, lowerChild + _childrenPerPage);
      for (int i = lowerChild; i < upperChild; i++) {
        dst.children.push_back(keyForIndex(nodeKey.terminology, inds[i]));
      }
      return dst;
    }

    void Editor::enableAll(const NodeKey& src) {
      auto srcData = nodeData(src);
      auto nodeIndex = NodeIndex(srcData.index);
      const auto& sol = _acc.solutionForNode(nodeIndex);
      NodeStateEnabledState change;
      change.nodeIndex = nodeIndex;

      std::vector<NodeStateEnabledState> changes;
      for (auto s: sol.getStateInds()) {
        change.stateIndex = s;
        change.value = true;
        changes.push_back(change);
      }
      _acc.changeEnabledStates(_problem.get(), changes);
    }

    std::set<Editor::NodeKey> Editor::getMatches(const Editor::NodeKey& src) const {
      if (src.terminology == TerminologyKey::Dst) {
        return {};
      }
      auto nodeIndex = NodeIndex(nodeData(src).index);
      const auto& sol = _acc.solutionForNode(nodeIndex);
      if (sol.allEnabled()) {
        return {};
      }

      std::set<Editor::NodeKey> dst;
      for (auto s: sol.getEnabledStateInds()) {
        dst.insert(keyForIndex(TerminologyKey::Dst, s.value));
      }
      return dst;
    }

    void Editor::changeEnabledState(const NodeKey& src, const std::vector<NodeKey>& dst, bool enabled) {
      CHECK(src.terminology == TerminologyKey::Src);

      auto srcData = nodeData(src);
      auto nodeIndex = NodeIndex(srcData.index);
      const auto& sol = _acc.solutionForNode(nodeIndex);

      auto allStateInds = sol.getStateInds();
      auto enabledInds = sol.getEnabledStateInds();

      LOG("Currently " << enabledInds.size() << " of " << allStateInds.size() << " are enabled.");

      std::map<StateIndex, TerminologyTreeLookup::NodeData> m;
      for (auto k: dst) {
        auto data = nodeData(k);
        m[StateIndex(data.index)] = data;
      }

      NodeStateEnabledState change;
      change.nodeIndex = nodeIndex;

      std::vector<NodeStateEnabledState> changes;
      if (enabled) {
        if (allStateInds.size() == enabledInds.size()) {
          LOG("Disable all except the chose ones");
          for (auto s: allStateInds) {
            change.stateIndex = s;
            change.value = m.count(s) == 1;
            changes.push_back(change);
          }
        } else {
          LOG("Enable the chosen ones");
          for (auto kv: m) {
            change.stateIndex = kv.first;
            change.value = true;
            changes.push_back(change);
          }
        }
      } else {
        auto remaining = enabledInds;
        for (const auto& kv: m) {
          auto f = remaining.find(kv.first);
          if (f != remaining.end()) {
            remaining.erase(f);
          }
        }

        if (remaining.empty()) {
          LOG("Re-enable all");
          for (auto s: allStateInds) {
            change.stateIndex = s;
            change.value = true;
            changes.push_back(change);
          }
        } else {
          LOG("Disable the chosen ones");
          for (const auto& kv: m) {
            change.stateIndex = kv.first;
            change.value = false;
            changes.push_back(change);
          }
        }
      }
      //for (const auto& dstKey: dst) {
      //  auto dstData = nodeData(dst);
      //auto stateIndex = StateIndex(dstData.index);
      //}

      int enabledCount = 0;
      for (const auto& c: changes) {
        if (c.value) {
          enabledCount++;
        }
      }

      LOG("Apply " << changes.size() << " changes: enable " << enabledCount << " and disable " << changes.size() - enabledCount);
      CHECK(0 < changes.size());
      _acc.changeEnabledStates(_problem.get(), changes);
    }

    std::string Editor::label(const std::vector<LangString>& labels) const {
      return findLabelInLanguage(labels, _setup->lang).value();
    }

    std::string Editor::label(const NodeKey& k) const {
      if (TerminologyTreeNode::childNodeKeyType()->hasSubType(k.key.type())) {
        return "broader " + label(k.withKey(k.key.withType(SkosConcept::CONCEPT_KEY_TYPE)));
      }
      auto c = conceptData(k);
      if (c == nullptr) {
        return k.key.toString();
      } else {
        return k.key.toString() + ": " + label(c->concept.prefLabels());
      }
    }

    void Editor::goToNode(const NodeKey& k) {
      if (k != currentNode()) {
        _currentPage = 0;
        _nodeHistory.push_back(k);
      }
    }

    Editor::NodeKey Editor::parent(const Editor::NodeKey& k) const {
      return keyForIndex(k.terminology, nodeData(k).parent);
    }

    std::vector<Editor::NodeKey> Editor::parents(const Editor::NodeKey& k) const {
      std::vector<Editor::NodeKey> dst;
      auto p = parent(k);
      while (p.key.isDefined()) {
        dst.push_back(p);
        p = parent(p);
      }
      std::reverse(dst.begin(), dst.end());
      return dst;
    }

    void Editor::goUp() {
      goToNode(parent(currentNode()));
    }

    void Editor::goBack() {
      if (canGoBack()) {
        _nodeHistory.pop_back();
        _currentPage = 0;
      }
    }


    void Editor::mainMenu(const MainMenuSettings& settings) {
      while (true) {
        TextRenderer r(&(std::cout));
        auto cnode = currentNode();
        auto luData = nodeData(cnode);
        auto concept = conceptData(currentNode());
        auto rConcept = r.deeper("  * ");
        Menu menu;
        BasicInputMatcher<NodeKey> conceptMatcher(&menu, &rConcept);
        BasicInputMatcher<std::string> otherCommands(&menu, &r);

        r.clear();
        r.startNewLine() << "*** Editing " <<  luData.key.toString() << " at level " << luData.depth;
        {
          auto parentKeys = parents(currentNode());
          if (0 < parentKeys.size()) {
            std::string indent = "";
            auto& dst = r.startNewLine() << "Parent nodes: ";
            for (const auto& p: parentKeys) {
              rConcept.startNewLine() << indent << label(p);
              indent += "  ";
            }
          }
        }
        r.startNewLine();
        r.startNewLine() << "CURRENT NODE: " << label(cnode);
        r.startNewLine();
        if (settings.nodeToAssign.key.isDefined()) {
          r.startNewLine() << "Node to assign: " << label(settings.nodeToAssign);
          r.startNewLine();
          otherCommands.delayedBind("c", "constrain", "Assign this node");
        }

        if (cnode.terminology == TerminologyKey::Src) {
          auto matches = getMatches(cnode);
          r.startNewLine();
          if (matches.size() == 0) {
            r.startNewLine() << "Not assigned.";
          } else {
            r.startNewLine() << "Assigned to " << matches.size() << " matches";
            for (const auto& k: matches) {
              rConcept.startNewLine() << label(k);
            }
          }
          r.startNewLine();
        }

        if (concept != nullptr) {
          auto r2 = r.deeper("  ");
          displayConceptInfo(concept, &r2);
        }

        otherCommands.delayedBind("e", "edit", "Edit correspondences");
        if (canGoBack()) {
          otherCommands.delayedBind("p", "previous", "Previous node");
        }
        if (0 < luData.depth) {
          otherCommands.delayedBind("u", "up", "Parent node (up)");
        }
        otherCommands.delayedBind("q", "quit", settings.quitDescription);
        {
          auto cd = childData(cnode);
          r.startNewLine() << "Narrower nodes, page " << _currentPage+1 << "/" << cd.pageCount << ":";
          for (const auto& child: cd.children) {
            conceptMatcher.bind("", child, label(child));
          }
          r.startNewLine();
        }
        otherCommands.finalize();

        while (true) {
          r.startNewLine() << "Choice or page number? ";
          std::string cmd;
          std::getline(std::cin, cmd);
          {
            auto k = conceptMatcher.match(cmd);
            if (k) {
              goToNode(k.value());
              break;
            }
          }{
            auto k = otherCommands.match(cmd);
            if (k) {
              if (k.value() == "quit") {
                return;
              } else if (k.value() == "previous") {
                goBack();
                break;
              } else if (k.value() == "up") {
                goUp();
                break;
              } else if (k.value() == "edit") {
                edit(&r, currentNode());
                break;
              } else if (k.value() == "constrain") {
                changeEnabledState(settings.nodeToAssign, {cnode}, true);
                return;
              }
            }
          }
          r.startNewLine() << "Try again.";
        }
      }
    }

    std::string renderLabels(const std::vector<LangString>& labels) {
      std::stringstream dst;
      bool sep = false;
      for (const auto& lab: labels) {
        if (sep) {
          dst << ", ";
        }
        dst << lab.value() << " (" << lab.lang() << ")";
        sep = true;
      }
      return dst.str();
    }

    void Editor::displayConceptInfo(const SkosTerminology::ConceptData* data, TextRenderer* dst) {
      dst->startNewLine() << "Preferred label(s): " << renderLabels(data->concept.prefLabels()) << std::endl;
      if (0 < data->concept.altLabels().size()) {
        dst->startNewLine() << "Alternative label(s): " << renderLabels(data->concept.altLabels()) << std::endl;
      }
    }

    class EditorTask : public IConconoptTask {
    public:
      typedef std::shared_ptr<IConconoptTask> Ptr;

      EditorTask(const std::string& workFilename) : _workFilename(workFilename) {}

      std::string taskName() const override {
        return "EditorTask";
      }

      void perform(ConconoptSetup* setup) override {
        auto acc = loadAcc(setup);
        LOG("Successfully loaded acc");
        Editor editor(setup, acc, _workFilename);
        editor.mainMenu();
      }
    private:
      std::string _workFilename;

      TreeStateOptimizationAccumulation<GaussianCost> loadAcc(ConconoptSetup* setup) const {
        {
          LOG("Loading work accumulator " << _workFilename);
          auto acc = loadAccumulator(_workFilename);
          if (!acc.empty()) {
            return acc;
          }
          LOG("No accumulator to load at " << _workFilename << ", load the original accumulator");
        }{
          auto acc = loadAccumulator(setup->accumulatedCostsFilename);
          if (!acc.empty()) {
            return acc;
          }
        }
        CRASH("No accumulator to load");
      }
    };

    IConconoptTask::Ptr editTask(ConconoptSetup* setup, const nlohmann::json& data) {
      std::string workFilename = data["work_filename"];
      return std::make_shared<EditorTask>(workFilename);
    }

    IConconoptTask::Ptr parseTask(ConconoptSetup* setup, const nlohmann::json& data) {
      std::set<std::string> tasksToIgnore{"plot_condition_number"};

      std::string taskType = data["type"];
      if (taskType == "accumulate_costs") {
        return parseAccumulateTask(data);
      } else if (taskType == "analyze_problem") {
        return analyzeProblemTask(data);
      } else if (taskType == "sample_solutions") {
        return sampleSolutionsTask(setup, data);
      } else if (taskType == "parameter_sweep") {
        return parameterSweepTask(data);
      } else if (taskType == "sweep_condition_number") {
        return sweepConditionNumberTask(setup, data);
      } else if (taskType == "sweep_comparison") {
        return sweepComparisonTask(setup, data);
      } else if (taskType == "editor") {
        return editTask(setup, data);
      } else if (0 < tasksToIgnore.count(taskType)) {
        return nullptr;
      } else {
        CRASH("Unknown task type '" << taskType << "'");
      }
    }


    std::map<std::string, std::vector<double>> loadSbertStringVectors(const std::string& filename) {
      std::ifstream file(filename);
      auto data = nlohmann::json::parse(file);
      std::map<std::string, std::vector<double>> dst = data;
      return dst;
    }

    class MatrixCost : public IConconoptCost {
    public:
      MatrixCost(int srcCount, int dstCount) :
        _srcCount(srcCount), _dstCount(dstCount), _data(srcCount*dstCount) {}

      double getStateCost(NodeIndex i, StateIndex j) const override {
        return _data[computeIndex(i.value, j.value)];
      }

      void setCost(int i, int j, double c) {
        _data[computeIndex(i, j)] = c;
      }

      int srcCount() const {return _srcCount;}
      int dstCount() const {return _dstCount;}

      std::string toString() const override {
        std::stringstream ss;
        ss << "MatrixCost(" << _srcCount << "x" << _dstCount << ")";
        return ss.str();
      }

      nlohmann::json analyze() const override {
        double sum = 0.0;
        double minCost = std::numeric_limits<double>::infinity();
        double maxCost = -std::numeric_limits<double>::infinity();
        for (int i = 0; i < _srcCount; i++) {
          for (int j = 0; j < _dstCount; j++) {
            double c = getStateCost(NodeIndex(i), StateIndex(j));
            maxCost = std::max(maxCost, c);
            minCost = std::min(minCost, c);
            sum += c;
          }
        }
        int elementCount = _srcCount*_dstCount;


        nlohmann::json dst;
        dst["type"] = "MatrixCost";
        dst["src_count"] = _srcCount;
        dst["dst_count"] = _dstCount;
        dst["element_count"] = elementCount;
        dst["total_cost"] = sum;
        dst["average_cost"] = sum/elementCount;
        dst["min_cost"] = minCost;
        dst["max_cost"] = maxCost;
        return dst;
      }
    private:
      int computeIndex(int i, int j) const {
        return i*_dstCount + j;
      }

      int _srcCount, _dstCount;
      std::vector<double> _data;
    };

    namespace {
      const std::vector<double>& lookupVectorForKey(
          const std::string& language,
          const std::map<std::string, std::vector<double>>& sbertData, const TerminologySetup& setup, const SkosKey& k) {
        static std::vector<double> emptyVector;

        auto conceptData = setup.terminology.find(k);
        if (conceptData == nullptr) {
          return emptyVector;
        }

        auto label = findLabelInLanguage(conceptData->concept.prefLabels(), language);
        if (label.value().empty()) {
          return emptyVector;
        }

        auto f = sbertData.find(label.value());
        if (f == sbertData.end()) {
          return emptyVector;
        }
        return f->second;
      }

      double computeVectorAngle(const std::vector<double>& a, const std::vector<double>& b, double defaultAngle) {
        size_t n = a.size();
        if (n != b.size() || n == 0) {
          return defaultAngle;
        }

        double a2 = 0.0;
        double b2 = 0.0;
        double ab = 0.0;
        for (int i = 0; i < n; i++) {
          auto x = a[i];
          auto y = b[i];
          a2 += x*x;
          b2 += y*y;
          ab += x*y;
        }
        if (a2 <= 0.0 || b2 <= 0.0) {
          return defaultAngle;
        }
        double denom = a2*b2;
        if (denom <= 0.0) {
          return defaultAngle;
        }
        double cosAngle = ab/sqrt(a2*b2);
        if (cosAngle <= -1.0) {
          return M_PI;
        } else if (1.0 <= cosAngle) {
          return 0.0;
        }
        return acos(cosAngle);
      }

      std::function<double(double)> angle2distanceFunctionFromMetric(const std::string& metric) {
        if (metric == "cossim") {
          return [](double angle) {return 0.5*(1 - cos(angle));};
        } else if (metric == "angle") {
          return [](double angle) {return angle;};
        } else if (metric == "norm") {
          return [](double angle) {
            double dx = cos(angle) - 1.0;
            double dy = sin(angle);
            return sqrt(std::max(0.0, dx*dx + dy*dy));
          };
        } else {
          CRASH("Unknown metric " << metric);
          return [](double x) {return x;};
        }
      }

      double constraintCost(const SkosKey& src, const SkosKey& dst) {
        //CRASH("Hej");
        if (src.type() == TerminologyTreeNode::rootKeyType() || dst.type() == TerminologyTreeNode::rootKeyType()) {
          /*if (src != dst) {
            LOG("Different: " << src.toString() << " and " << dst.toString());
          }
          CHECK(src == dst);*/
          return src == dst? 0 : 1.0;
        }
        return 0.0;
      }
    }

    IConconoptCost::Ptr parseSbertCost(const ConconoptSetup& partialSetup, const nlohmann::json& data) {
      auto angle2distance = angle2distanceFunctionFromMetric(data["distance_metric"]); // "cossim",
      double undefinedAngle = data["undefined_angle_radians"]; // 0.5,
      std::string filename = data["sbert_vectors_filename"]; //data/demo_occuaption_sni/occupation_sni_sbert.json"

      DOUT(undefinedAngle);
      DOUT(filename);

      auto sbertData = loadSbertStringVectors(filename);

      auto costf = std::make_shared<MatrixCost>(
          partialSetup.src.treeLookup->size(),
          partialSetup.dst.treeLookup->size());

      for (const auto& srcNode: partialSetup.src.treeLookup->nodeData()) {
        const auto& srcVector = lookupVectorForKey(partialSetup.lang, sbertData, partialSetup.src, srcNode.key);
        for (const auto& dstNode: partialSetup.dst.treeLookup->nodeData()) {
          const auto& dstVector = lookupVectorForKey(partialSetup.lang, sbertData, partialSetup.dst, dstNode.key);
          double angle = computeVectorAngle(srcVector, dstVector, undefinedAngle);
          double cost = angle2distance(angle) + partialSetup.constraintPenalty*constraintCost(srcNode.key, dstNode.key);
          costf->setCost(srcNode.index, dstNode.index, cost);
        }
      }
      LOG("Done with cost of size " << costf->srcCount() << "x" << costf->dstCount());

      return costf;
    }

    IConconoptCost::Ptr parseCost(const ConconoptSetup& partialSetup, const nlohmann::json& data) {
      std::string costType = data["type"];
      if (costType == "sbert_cost") {
        return parseSbertCost(partialSetup, data);
      } else if (costType == "parent_child_reg_cost") {
        return std::make_shared<ParentChildRegCost>(partialSetup.dst.treeLookup);
      }
      CRASH("Cost not recognized: " << costType);
      return {};
    }

  }
  

  GaussianCost ConconoptProblem::getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const {
    double sum = 0.0;
    for (const auto kwc: _costs) {
      const auto& wc = kwc.second;
      sum += wc.weight*wc.cost->getStateCost(nodeIndex, stateIndex);
    }
    return GaussianCost(sum, 1.0);
  }

  GaussianCost ConconoptProblem::getTransitionCost(
      NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
      StateIndex childState, StateIndex parentState) const {
    double sum = 0.0;
    for (const auto kwc: _costs) {
      const auto& wc = kwc.second;
      sum += wc.weight*wc.cost->getTransitionCost(
          childNodeIndex, parentNodeIndex, childState, parentState);
    }
    return GaussianCost(sum, 0.0);
  }

  void ConconoptProblem::reportAccumulationProgress(int processedCount, int totalCount) {
    LOG("Done " << processedCount << " of " << totalCount);
  }

//  void to_json(nlohmann::json& j, const SkosKey& p) {
//    j = nlohmann::json{
//      {"type", p.type()->id()},
//      {"data", p.id()},
//    };
//  }
//
//  void from_json(const nlohmann::json& j, SkosKey& p) {
//    std::string type, data;
//    j.at("type").get_to(type);
//    j.at("data").get_to(data);
//    CRASH("Type must be mapped")
//    p = SkosKey();
//  }


  ConconoptSetup ConconoptSetup::loadFromConfig(const std::string& filename) {
    std::ifstream file(filename);
    auto data = nlohmann::json::parse(file);

    ConconoptSetup setup;
    setup.accumulatedCostsFilename = data["accumulated_costs_filename"];
    setup.src.loadFromJson(data["src_terminology"]);
    setup.dst.loadFromJson(data["dst_terminology"]);
    for (const auto& kwc: data["objective_function_terms"].items()) {
      std::string k = kwc.key();
      const auto& wc = kwc.value();
      Delayed<WeightedConconoptCost> costFunction([=]() {
        auto cost = parseCost(setup, wc);
        return WeightedConconoptCost(wc["weight"], cost);
      });
      setup.objectiveFunctionTerms[k] = costFunction;
    };
    setup.sampleCount = data["sample_count"];
    setup.sigma = data["sigma"];


    LOG("Loaded " << setup.objectiveFunctionTerms.size() << " terms.");
    for (const auto& kv: data["tasks"].items()) {
      auto task = parseTask(&setup, kv.value());
      if (task) {
        setup.taskMap[kv.key()] = task;
      }
    }
    for (const auto& kv: setup.taskMap) {
      kv.second->configure(&setup);
    }
    return setup;
  }

  TreeStateOptimizationAccumulation<GaussianCost> ConconoptSetup::accumulate(
      const std::shared_ptr<ConconoptProblem>& problem) const {
    TreeStateOptimizerSettings settings;
    return TreeStateOptimizationAccumulation<GaussianCost>(problem.get(), settings);
  }

  std::map<std::string, WeightedConconoptCost> ConconoptSetup::realizeTerms() {
    std::map<std::string, WeightedConconoptCost> weightedCosts;
    for (auto& kv : objectiveFunctionTerms) {
      weightedCosts[kv.first] = kv.second.get();
    }
    return weightedCosts;
  }

  std::shared_ptr<ConconoptProblem> ConconoptSetup::initializeOptimizationProblem(
      const std::map<std::string, WeightedConconoptCost>& terms) {
    return std::make_shared<ConconoptProblem>(src.treeLookup, dst.treeLookup, terms);
  }

  std::shared_ptr<ConconoptProblem> ConconoptSetup::initializeOptimizationProblem() {
    return initializeOptimizationProblem(realizeTerms());
  }

  namespace {
    std::set<std::string> combineTags(const std::set<std::string>& a, const std::set<std::string>& b) {
      std::set<std::string> dst;
      for (const auto& s: {a, b}) {
        for (const auto& x: s) {
          dst.insert(x);
        }
      }
      return dst;
    }
  }

  ConceptPair ConceptPair::combine(const ConceptPair& other) const {
    CHECK(src == other.src);
    CHECK(dst == other.dst);
    return {src, dst, combineTags(tags, other.tags), count + other.count};
  }

  ConceptMapping  ConceptMapping::compact() const {
    std::map<std::pair<SkosKey, SkosKey>, ConceptPair> m;
    for (const auto& p: srcDstPairs) {
      auto k = p.key();
      if (m.count(k) == 0) {
        m[k] = p;
      } else {
        m[k] = m[k].combine(p);
      }
    }

    ConceptMapping dst;
    dst.cost = cost;
    for (const auto& kv: m) {
      dst.srcDstPairs.push_back(kv.second);
    }
    return dst;
  }

  ConceptMapping ConceptMapping::merge(const std::vector<ConceptMapping>& other) {
    ConceptMapping dst;
    dst.cost = 0.0;
    for (const auto& x: other) {
      dst.cost += x.cost;
      for (const auto& p: x.srcDstPairs) {
        dst.srcDstPairs.push_back(p);
      }
    }
    return dst.compact();
  }


  ConconoptSetup::ConconoptSetup() {
    rng.seed(std::chrono::system_clock::now().time_since_epoch().count());
    src.addUndefinedChildNodes = false;
    dst.addUndefinedChildNodes = true;
  }

  ConceptMapping ConconoptSetup::conceptMappingFromSolution(
      const TreeStateOptimizationSolution<GaussianCost>& sol, const std::set<ConceptPair::Tag>& tags) const {
    ConceptMapping result;
    result.cost = sol.cost.mu();
    for (const auto& kv: sol.node2stateMap) {
      result.srcDstPairs.push_back(ConceptPair{
        src.treeLookup->get(kv.first.value).key,
        dst.treeLookup->get(kv.second.value).key,
        tags
      });
    }
    return result;
  }

  void ConconoptSetup::performTask(const std::string& taskKey) {
    auto f = taskMap.find(taskKey);
    if (f == taskMap.end()) {
      CRASH("No task '" << taskKey << "'");
    } else {
      f->second->perform(this);
    }
  }

  /*namespace {
    struct ConceptInfo {
      SkosKey key;
      std::string label;
    };

    struct CsvRow {
      ConceptInfo src, dst;
      int count;
    };
  }*/

  namespace {


    struct ConceptPairGroup {
      std::vector<ConceptPair> pairs;
      int totalCount = 0;

      const ConceptPair& first() const {
        return pairs[0];
      }

      double certainty() const {
        return double(first().count)/totalCount;
      }

      ConceptPairGroup() {}
      ConceptPairGroup(const std::vector<ConceptPair>& ps) : pairs(ps) {
        std::sort(pairs.begin(), pairs.end(), [](const ConceptPair& a, const ConceptPair& b) {
          return a.count >= b.count;
        });
        for (const auto& x: pairs) {
          totalCount += x.count;
        }
      }
    };

    struct ConceptPairGroupOrdering {
      const ConconoptSetup* setup;

      ConceptPairGroupOrdering(const ConconoptSetup* s) : setup(s) {}

      std::tuple<int, double, int> makeTuple(const ConceptPairGroup& p) const {
        return {-p.certainty(), setup->src.treeLookup->get(p.first().src).depth, p.pairs.size()};
      }

      bool operator()(const ConceptPairGroup& a, const ConceptPairGroup& b) const {
        return makeTuple(a) < makeTuple(b);
      }
    };

    std::string percentage(double x) {
      std::stringstream ss;
      ss << int(round(100*x)) << "%";
      return ss.str();
    }

    std::string toString(int i) {
      std::stringstream ss;
      ss << i;
      return ss.str();
    }


    std::string joinByComma(const std::set<std::string>& s) {
      std::stringstream ss;
      bool sep = false;
      for (const auto& x: s) {
        if (sep) {
          ss << ", ";
        }
        ss << x;
        sep = true;
      }
      return ss.str();
    }
  }

  void ConconoptSetup::writeConceptMappingCSV(const std::string& filename, const ConceptMapping& cm) const {
    std::map<SkosKey, std::vector<ConceptPair>> groupMap;
    for (const auto& x: cm.srcDstPairs) {
      auto k = x.src;
      if (groupMap.count(k) == 0) {
        groupMap[k] = {x};
      } else {
        groupMap[k].push_back(x);
      }
    }

    std::vector<ConceptPairGroup> groups;
    for (const auto& kv: groupMap) {
      groups.push_back({kv.second});
    }
    std::sort(groups.begin(), groups.end(), ConceptPairGroupOrdering(this));

    std::ofstream file(filename);
    auto writer = csv::make_csv_writer(file);
    writer << std::vector<std::string>{
      "Src index",
      "Src id",
      "Dst id",
      "Src level",
      "Dst level",
      "Src label",
      "Dst label",
      "Tags",
      "Percentage",
      "Count"};
    for (const auto& group: groups) {
      for (const auto& pair: group.pairs) {
        auto srcK = pair.src;
        auto dstK = pair.dst;
        std::vector<std::string> row{
          toString(src.treeLookup->get(srcK).index),
          serializeSkosKey(srcK),
          serializeSkosKey(dstK),
          toString(src.treeLookup->get(srcK).depth),
          toString(dst.treeLookup->get(dstK).depth),
          conceptLabel(src, srcK, lang),
          conceptLabel(dst, dstK, lang),
          joinByComma(pair.tags),
          percentage(double(pair.count)/group.totalCount),
          toString(pair.count)
        };
        writer << row;
      }

    }

  }

  void testConconopt() {
    auto ak = SkosConcept::makeKey("a");
    auto bk = SkosConcept::makeKey("b");
    auto ck = SkosConcept::makeKey("c");
    auto dk = SkosConcept::makeKey("d");
    auto ek = SkosConcept::makeKey("e");
    
    std::vector<SkosConcept> concepts{
      SkosConcept(ak, {LangString("Datarelaterade yrken", "sv")}, {}, {}, {}),
      SkosConcept(bk, {LangString("Mjukvaruutvecklare", "sv")}, {}, {ak}, {dk}),
      SkosConcept(ck, {LangString("Hårdvaruutvecklare", "sv")}, {}, {ak}, {}),
      SkosConcept(dk, {LangString("Clojureprogrammerare", "sv")}, {}, {}, {}),
      SkosConcept(ek, {LangString("Yrken inom jordbruk och natur", "sv")}, {}, {}, {})
    };
    
    auto terminology = SkosTerminology::fromConcepts(concepts);
    CHECK(terminology.conceptKeys().size() == 5);
    CHECK(terminology.rootKeys().size() == 2);
    CHECK(terminology.leafKeys().size() == 3);

    {
      TerminologySetup setup;
      setup.addRoot = true;
      setup.terminology = terminology;

      auto tree = setup.buildTree();
      CHECK(tree->key() == TerminologyTreeNode::rootKey());

      auto lu = TerminologyTreeLookup(tree);
      {
        auto path = lu.summarizePath(lu.get(bk).index, lu.get(ck).index);
        CHECK(path.minimumCommonNode == lu.get(ak).index);
        CHECK(path.ascentSteps == 1);
        CHECK(path.descentSteps == 1);
      }
      {
        auto path = lu.summarizePath(lu.get(dk).index, lu.get(ck).index);
        CHECK(path.minimumCommonNode == lu.get(ak).index);
        CHECK(path.ascentSteps == 2);
        CHECK(path.descentSteps == 1);
      }
      {
        auto regcost = ParentChildRegCost(std::make_shared<TerminologyTreeLookup>(lu));
        CHECK(0.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(bk).index), StateIndex(lu.get(ak).index)));
        CHECK(1.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(ak).index), StateIndex(lu.get(bk).index)));
        CHECK(1.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(ak).index), StateIndex(lu.get(ak).index)));
        CHECK(1.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(bk).index), StateIndex(lu.get(bk).index)));
        CHECK(0.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(dk).index), StateIndex(lu.get(ak).index)));
        CHECK(1.0 == regcost.getTransitionCost({}, {}, StateIndex(lu.get(bk).index), StateIndex(lu.get(ck).index)));
      }
    }{

      ConceptMapping a;
      a.cost = 3.0;
      a.srcDstPairs.push_back({SkosConcept::makeKey("a"), SkosConcept::makeKey("b"), {"optimum"}, 3});
      a.srcDstPairs.push_back({SkosConcept::makeKey("a"), SkosConcept::makeKey("b"), {}, 4});

      {
        auto a2 = a.compact();
        CHECK(a2.cost == 3.0);
        CHECK(a2.srcDstPairs.size() == 1);
        CHECK(a2.srcDstPairs[0].count == 7);
        CHECK(a2.srcDstPairs[0].src.id() == "a");
      }

      ConceptMapping b;
      b.cost = 10.0;
      b.srcDstPairs.push_back({SkosConcept::makeKey("a"), SkosConcept::makeKey("c"), {}, 7});

      auto c = ConceptMapping::merge({a, b});
      CHECK(c.cost == 13.0);
      CHECK(c.srcDstPairs.size() == 2);
    }{
      auto cm = DiscreteWeightMapping::centMapping();
      CHECK(10.0 == cm.unmap(100));
      CHECK(1.0 == cm.unmap(0));
      for (int i = -1000; i <= 1000; i++) {
        CHECK(i == cm.map(cm.unmap(i)));
      }

      CHECK("pos100" == cm.encodeValue(10.0));
      DOUT(cm.encodeValue(1.0));
      CHECK("zer0" == cm.encodeValue(1.0));
      CHECK("neg100" == cm.encodeValue(0.1));

      CHECK(10.0 == cm.decodeString("pos100"));
      CHECK(1.0 == cm.decodeString("zer0"));
      CHECK(0.1 == cm.decodeString("neg100"));
    }{
      auto parsed = parseConceptCommand({"enable", "disable"}, "enable xyz");
      CHECK(parsed.command == "enable");
      CHECK(parsed.concepts.size() == 3);
      CHECK(parsed.concepts[2] == "z");
    }
  }
}

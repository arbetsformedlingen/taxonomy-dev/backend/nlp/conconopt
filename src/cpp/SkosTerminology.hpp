#ifndef __SKOS_TERMINOLOGY_HPP__
#define __SKOS_TERMINOLOGY_HPP__

#include <string>
#include <vector>
#include <tuple>
#include <iosfwd>
#include <map>
#include "utils.hpp"
#include <mutex>
#include <memory>

namespace ccopt {

class LangString {
public:
  LangString() {}
  LangString(const std::string& value, const std::string& lang) : _value(value), _lang(lang) {}

  const std::string& value() const {return _value;}
  const std::string& lang() const {return _lang;}
private:
  std::string _value, _lang;
};

class KeyType {
public:
  static const KeyType* BASE;
  
  KeyType(const std::string& id, const KeyType* parent) : _id(id), _parent(parent) {}

  bool hasSubType(const KeyType* other) const {
    if (other == nullptr) {
      return false;
    } else if (other == this) {
      return true;
    } else {
      return hasSubType(other->_parent);
    }
  }

  bool operator<(const KeyType& other) const {
    return _id < other._id;
  }

  const KeyType* parent() const {
    return _parent;
  }

  const std::string& id() const {
    return _id;
  }
private:
  std::string _id;
  const KeyType* _parent;
};

class KeyTypeFactory {
public:
  KeyType* make(const std::string& id, const KeyType* parent = KeyType::BASE);
  KeyType* find(const std::string& id);
  std::map<std::string, std::shared_ptr<KeyType>> getRegistry();

  static KeyTypeFactory* instance();
private:
  std::mutex _m;
  std::map<std::string, std::shared_ptr<KeyType>> _registry;
};
  
class SkosKey {
public:
  SkosKey() : _type(nullptr) {}
  SkosKey(const KeyType* type, const std::string& id = "") : _type(type), _id(id) {}

  bool isDefined() const {return _type != nullptr;}

  std::tuple<const KeyType*, std::string> makeTuple() const {
    return {_type, _id};
  }

  bool operator<(const SkosKey& other) const {
    return makeTuple() < other.makeTuple();
  }

  bool operator==(const SkosKey& other) const {
    return makeTuple() == other.makeTuple();
  }
  
  bool operator!=(const SkosKey& other) const {
    return makeTuple() != other.makeTuple();
  }
  
  std::string toString() const {
    return "SkosKey(" + _type->id() + ":" + _id + ")";
  }

  const KeyType* type() const {return _type;}
  const std::string& id() const {return _id;}

  SkosKey withType(const KeyType* type) const {
    return SkosKey(type, _id);
  }
private:
  const KeyType* _type;
  std::string _id;
};

std::string serializeSkosKey(const SkosKey& src);
SkosKey deserializeSkosKey(const std::string& string);
  
LangString findLabelInLanguage(const std::vector<LangString>& labels, const std::string& lang);

class SkosConcept {
public:
  static const KeyType* CONCEPT_KEY_TYPE;

  static SkosKey makeKey(const std::string& id) {
    return SkosKey(CONCEPT_KEY_TYPE, id);
  }

  SkosConcept() {}
  SkosConcept(SkosKey k, const std::vector<LangString>& prefLabels, const std::vector<LangString>& altLabels, const std::vector<SkosKey>& broader, const std::vector<SkosKey>& narrower) : _key(k), _prefLabels(prefLabels), _altLabels(altLabels), _broader(broader), _narrower(narrower) {}

  bool isDefined() const {
    return _key.isDefined();
  }

  const SkosKey& key() const {
    return _key;
  }

  const std::vector<LangString>& prefLabels() const {
    return _prefLabels;
  }
  
  const std::vector<LangString>& altLabels() const {
    return _altLabels;
  }
  
  const std::vector<SkosKey>& broader() const {
    return _broader;
  }
  
  const std::vector<SkosKey>& narrower() const {
    return _narrower;
  }

  SkosConcept withBroader(const std::vector<SkosKey>& broader) const {
    auto dst = *this;
    dst._broader = broader;
    return dst;
  }
  
  SkosConcept withNarrower(const std::vector<SkosKey>& narrower) const {
    auto dst = *this;
    dst._narrower = narrower;
    return dst;
  }
private:
  SkosKey _key;
  std::vector<LangString> _prefLabels, _altLabels;
  std::vector<SkosKey> _broader, _narrower;
};
  
class SkosTerminology {
public:
  struct ConceptData {
    SkosConcept concept;
    int depth = -1;
  };

  SkosTerminology() {}
  
  static SkosTerminology loadFromJsonld(const std::string& filename);
  static SkosTerminology fromConcepts(const std::vector<SkosConcept>& concepts);

  const std::vector<SkosKey>& conceptKeys() const {
    return _conceptKeys;
  }
  
  const std::vector<SkosKey>& rootKeys() const {
    return _rootKeys;
  }
  
  const std::vector<SkosKey>& leafKeys() const {
    return _leafKeys;
  }

  const ConceptData& operator[](const SkosKey& k) const;

  const ConceptData* find(const SkosKey& k) const;
private:
  SkosTerminology(const std::vector<SkosConcept>& concepts);
  
  std::vector<SkosKey> _rootKeys, _leafKeys, _conceptKeys;
  std::map<SkosKey, ConceptData> _conceptMap;
};

  
void testSkosTerminology();

}

std::ostream& operator<<(std::ostream& s, const ccopt::LangString& x);
std::ostream& operator<<(std::ostream& s, const ccopt::SkosKey& k);
std::ostream& operator<<(std::ostream& s, const ccopt::SkosConcept& c);

#endif

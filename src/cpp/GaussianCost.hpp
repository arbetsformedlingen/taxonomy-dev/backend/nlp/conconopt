/*
 * GaussianCost.h
 *
 *  Created on: Oct 21, 2022
 *      Author: jonas
 */

#ifndef GAUSSIANCOST_HPP_
#define GAUSSIANCOST_HPP_

#include <nlohmann/json.hpp>
#include <iosfwd>

namespace ccopt {

class GaussianCost {
public:
  GaussianCost(double mu_=0.0, double sigmaSquared_=0.0) : _mu(mu_), _sigmaSquared(sigmaSquared_) {}

  GaussianCost operator+(const GaussianCost& other) const {
    return GaussianCost(_mu + other._mu, _sigmaSquared + other._sigmaSquared);
  }

  GaussianCost operator-(const GaussianCost& other) const {
    return GaussianCost(_mu - other._mu, _sigmaSquared + other._sigmaSquared);
  }

  GaussianCost operator-() const {
    return GaussianCost(-_mu, _sigmaSquared);
  }

  bool operator<(const GaussianCost& other) const {
    return _mu < other._mu;
  }

  bool operator==(const GaussianCost& other) const {
    return _mu == other._mu; // What about sigma?
  }

  bool operator!=(const GaussianCost& other) const {
    return _mu != other._mu; // What about sigma?
  }

  double mu() const {return _mu;}
  double sigmaSquared() const {return _sigmaSquared;}

  double sigma() const {return _sigmaSquared <= 0.0? 0.0 : sqrt(_sigmaSquared);}

  struct LessThanAnalysis {
    double probabilityIsLessThan = 0.5;
    double rightProbabilityBound = 0.0;

    static GaussianCost::LessThanAnalysis absolutelyCertainly(bool value);
    static GaussianCost::LessThanAnalysis prob(double rightProbBound);

    bool isCertain() const {
      return (probabilityIsLessThan == 1.0) || (probabilityIsLessThan == 0.0);
    }
  };

  operator double() const {
    return _mu;
  }

  LessThanAnalysis analyzeLessThan(const GaussianCost& other) const;

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(GaussianCost, _mu, _sigmaSquared)
private:
  double _mu = 0.0;
  double _sigmaSquared = 0.0;
};

std::ostream& operator<<(std::ostream& s, const GaussianCost& c);
std::ostream& operator<<(std::ostream& s, const GaussianCost::LessThanAnalysis& c);

void testGaussianCost();

}

namespace std {

inline ccopt::GaussianCost abs(const ccopt::GaussianCost& x) {
  return 0 <= x.mu()? x : -x;
}

}


#endif /* GAUSSIANCOST_HPP_ */

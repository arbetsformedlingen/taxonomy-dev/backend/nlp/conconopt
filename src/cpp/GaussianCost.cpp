/*
 * GaussianCost.cpp
 *
 *  Created on: Oct 21, 2022
 *      Author: jonas
 */

#include <iostream>
#include "GaussianCost.hpp"
#include "utils.hpp"

namespace ccopt {

namespace {

// https://en.cppreference.com/w/cpp/numeric/math/erfc
double normalCDF(double x) { // Phi(-∞, x) aka N(x)
  return std::erfc(-x/std::sqrt(2))/2;
}

}

GaussianCost::LessThanAnalysis GaussianCost::LessThanAnalysis::absolutelyCertainly(bool value) {
  LessThanAnalysis dst;
  dst.rightProbabilityBound = (value? 1 : -1)*std::numeric_limits<double>::infinity();
  dst.probabilityIsLessThan = value? 1.0 : 0.0;
  return dst;
}

GaussianCost::LessThanAnalysis GaussianCost::LessThanAnalysis::prob(double rightProbBound) {
  LessThanAnalysis dst;
  dst.rightProbabilityBound = rightProbBound;
  dst.probabilityIsLessThan = normalCDF(rightProbBound);
  return dst;
}

GaussianCost::LessThanAnalysis GaussianCost::analyzeLessThan(const GaussianCost& other) const {
  // x < y <=> x - y < 0 <=> -x + y > 0

  // x = mu + p*sigma
  // y = mu_other + q*sigma_other

  // -x + y > 0 <=> -(mu + p*sigma) + mu_other + q*sigma_other > 0 <=>
  // -p*sigma + q*sigma_other >= mu - mu_other <-- This is the plane in normal coordinates

  double nx = -sigma();
  double ny = other.sigma();
  double offset = mu() - other.mu();
  double norm2 = nx*nx + ny*ny;

  return (norm2 <= 0)?
    LessThanAnalysis::absolutelyCertainly(_mu < other._mu)
    :
    LessThanAnalysis::prob(-offset/norm2);
}

std::ostream& operator<<(std::ostream& s, const GaussianCost& c) {
  s << "GaussianCost(mu=" << c.mu() << ", sigma^2=" << c.sigmaSquared() << ")";
  return s;
}

std::ostream& operator<<(std::ostream& s, const GaussianCost::LessThanAnalysis& c) {
  s << "LessThanAnalysis(prob=" << c.probabilityIsLessThan << ", rightBd=" << c.rightProbabilityBound << ")";
  return s;
}

void testGaussianCost() {
  {
    auto a = GaussianCost(3.0, 4.0);
    auto b = GaussianCost(100.0, 1000.0);

    CHECK(a < b);
    CHECK(!(b < a));

    auto c = a + b;
    CHECK(c.mu() == 103.0);
    CHECK(c.sigmaSquared() == 1004.0);
  }{
    auto a = GaussianCost(3.0, 0.0);
    auto b = GaussianCost(4.0, 0.0);
    CHECK(a.analyzeLessThan(b).probabilityIsLessThan == 1.0);
    CHECK(b.analyzeLessThan(a).probabilityIsLessThan == 0.0);
  }{
    auto a = GaussianCost(3.0, 1.0);
    auto b = GaussianCost(4.0, 1.0);

    auto a2 = GaussianCost(3.0, 2.0);
    auto b2 = GaussianCost(4.0, 2.0);

    auto c = a.analyzeLessThan(b);
    CHECK(0.5 < c.probabilityIsLessThan);
    CHECK(c.probabilityIsLessThan < 1.0);

    auto c2 = a2.analyzeLessThan(b2);
    CHECK(c2.probabilityIsLessThan < c.probabilityIsLessThan);
    LOG(c);
    LOG(c2);
  }
}

}


#include <iostream>
#include <nlohmann/json.hpp>
#include "TreeStateOptimizer.hpp"
#include "SkosTerminology.hpp"
#include "Conconopt.hpp"
#include "GaussianCost.hpp"
#include "utils.hpp"
#include "ReduceTree.hpp"
#include <string>
#include <vector>

namespace {

  std::string readArg(const std::vector<std::string>& src, int i) {
    CHECK(i < src.size());
    return src[i];
  }
}

using namespace ccopt;

int main(int argc, const char** argv) {
  auto args = std::vector<std::string>(argv + 1, argv + argc);
  auto command = readArg(args, 0);
  if (command == "test") {
    ccopt::testTreeStateOptimizer();
    ccopt::testSkosTerminology();
    ccopt::testConconopt();
    ccopt::testGaussianCost();
    ccopt::testReduceTree();
    std::cout << "TESTS SUCCESSFUL" << std::endl;
  } else if (command == "run") {
    auto cfgFilename = readArg(args, 1);
    auto taskKey = readArg(args, 2);

    auto setup = ConconoptSetup::loadFromConfig(cfgFilename);
    std::cout << "Run task '" << taskKey << "'" << std::endl;
    setup.performTask(taskKey);
    std::cout << "Ran task '" << taskKey << "' defined in '" << cfgFilename << "'" << std::endl;
  } else {
    CRASH("Invalid commandline syntax");
  }
  
  return 0;
}

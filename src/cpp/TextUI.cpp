/*
 * TextUI.cpp
 *
 *  Created on: Nov 15, 2022
 *      Author: jonas
 */

#include "TextUI.hpp"
#include <iostream>

namespace ccopt {

std::ostream& TextRenderer::startNewLine() {
  *_dst << std::string("\n") << _prefix;
  return *_dst;
}

void TextRenderer::clear() {
  for (int i = 0; i < _clearCount; i++) {
    *_dst << "\n";
  }
}

}


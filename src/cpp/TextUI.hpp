/*
 * TextUI.hpp
 *
 *  Created on: Nov 15, 2022
 *      Author: jonas
 */

#ifndef TEXTUI_HPP_
#define TEXTUI_HPP_

#include <iosfwd>
#include <string>

namespace ccopt {

class TextRenderer {
public:
  TextRenderer(std::ostream* dst, const std::string& prefix="") : _dst(dst), _prefix(prefix) {}

  TextRenderer deeper(const std::string& extraPrefix) const {
    return TextRenderer(_dst, _prefix + extraPrefix);
  }

  void clear();

  std::ostream& startNewLine();
private:
  int _clearCount = 30;
  std::ostream* _dst;
  std::string _prefix;
};


}


#endif /* TEXTUI_HPP_ */

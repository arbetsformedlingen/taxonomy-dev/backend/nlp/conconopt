#include <iostream>
#include "TreeStateOptimizer.hpp"
#include "TreeStateOptimizerParameterizedNode.hpp"
#include <iostream>
#include <stdlib.h>
#include "utils.hpp"
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <limits>

#define CHECK_NEAR(a, b) CHECK(std::abs(a - b) < 1.0e-6)

std::ostream& operator<<(std::ostream& s, NodeIndex i) {
  s << "NodeIndex(" << i.value << ")";
  return s;
}

std::ostream& operator<<(std::ostream& s, StateIndex i) {
  s << "StateIndex(" << i.value << ")";
  return s;
}

template <typename T>
std::vector<T> makeInds(const std::vector<int>& src) {
  std::vector<T> dst;
  for (int i: src) {
    dst.push_back(T(i));
  }
  return dst;
}

std::vector<NodeIndex> nodeInds(const std::vector<int>& src) {
  return makeInds<NodeIndex>(src);
}

std::vector<StateIndex> stateInds(const std::vector<int>& src) {
  return makeInds<StateIndex>(src);
}

namespace ccopt {
  
  namespace {
    struct NodeDef {
      std::vector<NodeIndex> children;
      std::vector<StateIndex> states;
    };
    
    class TestProblem : public ITreeStateOptimizationProblem<double> {
    public:
      TestProblem(
        const std::vector<double>& statePositions,
        const std::unordered_map<NodeIndex, NodeDef>& nodeMap)
      
        : _statePositions(statePositions), _nodeMap(nodeMap) {
      
        std::vector<StateIndex> states;
        size_t n = statePositions.size();
        for (size_t i = 0; i < n; i++) {
          states.push_back(StateIndex(i));
        }

        for (auto& kv: _nodeMap) {
          if (kv.second.states.empty()) {
            kv.second.states = states;
          }
        }
      }

      NodeIndex getRootNode() const override {return NodeIndex(0);}
    
      const std::vector<NodeIndex>& getChildren(NodeIndex nodeIndex) const override {return _nodeMap.find(nodeIndex)->second.children;}
    
      const std::vector<StateIndex>& getStates(NodeIndex nodeIndex) const override {return _nodeMap.find(nodeIndex)->second.states;}
    
      double getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const override {
        return 0.0;
      }
    
      double getTransitionCost(
        NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
        StateIndex childState, StateIndex parentState) const override {
        return std::abs(1 - (_statePositions[childState.value] - _statePositions[parentState.value]));
      }
    
    private:
      std::vector<double> _statePositions;
      std::unordered_map<NodeIndex, NodeDef> _nodeMap;
    };  
  }
  
  void checkSol(const TreeStateOptimizationSolution<double>& sol) {
    CHECK(sol.cost == 0.0);
    CHECK(sol.node2stateMap.at(NodeIndex(4)) == StateIndex(7));
    CHECK(sol.node2stateMap.at(NodeIndex(7)) == StateIndex(7));
    CHECK(sol.node2stateMap.at(NodeIndex(0)) == StateIndex(2));
    CHECK(sol.node2stateMap.at(NodeIndex(1)) == StateIndex(5));
    CHECK(sol.node2stateMap.at(NodeIndex(2)) == StateIndex(7));
    CHECK(sol.node2stateMap.at(NodeIndex(3)) == StateIndex(5));
    CHECK(sol.node2stateMap.at(NodeIndex(5)) == StateIndex(5));
    CHECK(sol.node2stateMap.at(NodeIndex(80)) == StateIndex(7));
    CHECK(sol.node2stateMap.size() == 8);
  }

  struct NodeChainState {
    StateIndex stateIndex;
    double stateCost = 0.0;
    double position = 0.0;

    NodeChainState() {}
    NodeChainState(StateIndex i, double c, double pos) : stateIndex(i), stateCost(c), position(pos) {}
  };

  struct NodeChainData {
    NodeIndex nodeIndex;
    std::vector<NodeChainState> states;
    std::vector<StateIndex> stateInds;
    std::map<StateIndex, NodeChainState> stateMap;

    NodeChainData() {}
    NodeChainData(NodeIndex i, const std::vector<NodeChainState>& s) : nodeIndex(i), states(s) {
      for (const auto& state: s) {
        stateInds.push_back(state.stateIndex);
        stateMap[state.stateIndex] = state;
      }
    }
  };

  class NodeChainProblem : public ITreeStateOptimizationProblem<double> {
  public:
    NodeChainProblem(const std::vector<NodeChainData>& data) : _data(data) {
      for (size_t i = 0; i < data.size(); i++) {
        const auto& x = data[i];
        _nodeMap[x.nodeIndex] = x;
        int next = i + 1;
        _childMap[x.nodeIndex] = next < data.size()? std::vector<NodeIndex>{data[next].nodeIndex} : std::vector<NodeIndex>{};
      }
    }

    NodeIndex getRootNode() const override {return _data.front().nodeIndex;}

    const std::vector<NodeIndex>& getChildren(NodeIndex nodeIndex) const override {
      return _childMap.at(nodeIndex);
    }

    const std::vector<StateIndex>& getStates(NodeIndex nodeIndex) const override {
      return _nodeMap.at(nodeIndex).stateInds;
    }

    double getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const override {
      return _nodeMap.at(nodeIndex).stateMap.at(stateIndex).stateCost;
    }

    double getTransitionCost(
          NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
          StateIndex childState, StateIndex parentState) const override {
      auto childPos = _nodeMap.at(childNodeIndex).stateMap.at(childState).position;
      auto pos = _nodeMap.at(parentNodeIndex).stateMap.at(parentState).position;
      return std::abs(childPos - pos - 1.0);
    }
  private:
    std::vector<NodeChainData> _data;
    std::map<NodeIndex, NodeChainData> _nodeMap;
    std::map<NodeIndex, std::vector<NodeIndex>> _childMap;
  };

  struct StateDef {
    StateIndex index;
    double cost = 0.0;
    double position = 0.0;
  };

  class TestNode {
  public:
    typedef std::shared_ptr<TestNode> Ptr;

    NodeIndex index;
    std::vector<StateDef> states;
    std::vector<Ptr> children;

    TestNode(NodeIndex i, const std::vector<StateDef>& s, const std::vector<Ptr>& ch)
    : index(i), states(s), children(ch) {}

    TestNode* find(NodeIndex i) {
      if (i == index) {
        return this;
      }
      for (const auto& c: children) {
        auto f = c->find(i);
        if (f != nullptr) {
          return f;
        }
      }
      return nullptr;
    }

    void attachChild(NodeIndex parent, const TestNode::Ptr& child) {
      auto f = find(parent);
      if (f != nullptr) {
        f->children.push_back(child);
      }
    }
  };

  TestNode::Ptr initNode(int index, const std::vector<StateDef>& states) {
    return std::make_shared<TestNode>(NodeIndex(index), states, std::vector<TestNode::Ptr>());
  }

  class TestNodeProblem : public ITreeStateOptimizationProblem<double> {
  public:
    TestNodeProblem(const TestNode::Ptr& root) : _root(root) {
      traverse(root);
    }

    NodeIndex getRootNode() const override {
      return _root->index;
    }

    const std::vector<NodeIndex>& getChildren(NodeIndex nodeIndex) const override {
      return _children.at(nodeIndex);
    }

    const std::vector<StateIndex>& getStates(NodeIndex nodeIndex) const override {
      return _states.at(nodeIndex);
    }

    double getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const override {
      return _stateData.at(nodeIndex).at(stateIndex).cost;
    }

    double getTransitionCost(
        NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
        StateIndex childState, StateIndex parentState) const override {
      return std::abs(_stateData.at(childNodeIndex).at(childState).position - _stateData.at(parentNodeIndex).at(parentState).position);
    }

    const TestNode::Ptr& root() const {return _root;}
    const std::unordered_set<NodeIndex> nodeSet() const {
      return _nodeSet;
    }

    double solutionCount() const {
      double n = 1;
      for (NodeIndex i: _nodeSet) {
        n *= getStates(i).size();
      }
      return n;
    }

    bool solutionCountLessThan(int max) const {
      int n = 1;
      for (NodeIndex i: _nodeSet) {
        n *= getStates(i).size();
        if (n >= max) {
          return false;
        }
      }
      return true;
    }
  private:
    TestNode::Ptr _root;
    std::unordered_set<NodeIndex> _nodeSet;
    std::map<NodeIndex, std::map<StateIndex, StateDef>> _stateData;
    std::map<NodeIndex, std::vector<StateIndex>> _states;
    std::map<NodeIndex, std::vector<NodeIndex>> _children;

    void traverse(const TestNode::Ptr& node) {
      CHECK(_nodeSet.count(node->index) == 0);
      _nodeSet.insert(node->index);

      {
        std::map<StateIndex, StateDef> stateMap;
        std::vector<StateIndex> states;
        for (const auto& s: node->states) {
          states.push_back(s.index);
          stateMap[s.index] = s;
        }
        _states[node->index] = states;
        _stateData[node->index] = stateMap;
      }
      {
        std::vector<NodeIndex> children;
        for (const auto& c: node->children) {
          children.push_back(c->index);
        }
        _children[node->index] = children;
      }{
        for (const auto& c: node->children) {
          traverse(c);
        }
      }
    }
  };

  TreeStateOptimizationSolution<double> bruteForceSolve(TestNodeProblem* problem, std::map<NodeIndex, std::vector<StateIndex>> constraints = {}) {
    std::vector<NodeIndex> allNodes(problem->nodeSet().begin(), problem->nodeSet().end());
    std::vector<std::unordered_map<NodeIndex, StateIndex>> partialSolutions{{}};
    int counter = 0;
    for (auto nodeIndex: allNodes) {
      std::vector<std::unordered_map<NodeIndex, StateIndex>> next;
      auto f = constraints.find(nodeIndex);
      std::vector<StateIndex> states = f == constraints.end()? problem->getStates(nodeIndex) : f->second;
      for (auto state: states) {
        for (auto oldSol: partialSolutions) {
          auto newSol = oldSol;
          newSol[nodeIndex] = state;
          next.push_back(newSol);
        }
      }
      partialSolutions = next;
      counter++;
    }

    TreeStateOptimizationSolution<double> best;
    for (auto sol: partialSolutions) {
      best = std::min(best, TreeStateOptimizationSolution<double>(sol, problem));
    }
    return best;
  }

  void testForNode(const TestNode::Ptr& node, int maxBruteforceSolutionCount) {
    TestNodeProblem problem(node);
    //LOG("\n********************** TESTING WITH " << problem.nodeSet().size() << " nodes.");
    TreeStateOptimizationAccumulation<double> acc(&problem, {});
    bool brute = problem.solutionCountLessThan(maxBruteforceSolutionCount);
    auto overallOpt = acc.solve();
    if (brute) {
      auto opt2 = bruteForceSolve(&problem);
      CHECK_NEAR(overallOpt.cost, opt2.cost);
      CHECK(0 < problem.nodeSet().size());
      for (auto node: problem.nodeSet()) {
        CHECK(overallOpt.node2stateMap[node] == opt2.node2stateMap[node]);
      }
      LOG("Brute force check OK for tree rooted at " << node->index << " with " << problem.nodeSet().size() << " nodes");
    }
    for (auto node: problem.nodeSet()) {
      auto states = problem.getStates(node);
      auto cacc = acc;

      ParameterizeForNode<double> param(&problem, &acc, node);

      for (auto state: states) {
        param.setState(&problem, state);
        double paramCost = param.minimizeTop().cost;

        std::vector<NodeStateEnabledState> changes;
        for (auto s2: states) {
          NodeStateEnabledState change;
          change.nodeIndex = node;
          change.stateIndex = s2;
          change.value = s2 == state;
          changes.push_back(change);

        }
        cacc.changeEnabledStates(&problem, changes);

        auto cacc2 = acc;
        cacc2.changeEnabledStates(&problem, changes);
        CHECK_NEAR(cacc.solve().cost, cacc2.solve().cost);
        auto pmin = param.minimizeBottom();
        CHECK_NEAR(cacc.solve().cost, pmin.cost);
        CHECK(pmin.state == state);
      }

    }

  }


  void attachChild(const TestNode::Ptr& node, int parent, const TestNode::Ptr& child) {
    LOG("attachChild " << child->index);
    node->attachChild(NodeIndex(parent), child);
    testForNode(node, 100000);
  }


  void testTreeStateOptimizer() {
    {

      std::vector<NodeChainData> data{
        NodeChainData(NodeIndex(126), {NodeChainState(StateIndex(-353), 2.3, 0.1), NodeChainState(StateIndex(90), 2.4, 10.5)}),
        NodeChainData(NodeIndex(-43), {NodeChainState(StateIndex(1000), 1.3, 1.1), NodeChainState(StateIndex(1001), 1.2, 11.6)}),
      };
      NodeChainProblem problem(data);
      TreeStateOptimizationAccumulation<double> acc(&problem, TreeStateOptimizerSettings());

      {
        auto opt = acc.solve();
        CHECK(opt.node2stateMap.at(NodeIndex(126)) == StateIndex(-353));
        CHECK(opt.node2stateMap.at(NodeIndex(-43)) == StateIndex(1000));
        CHECK_NEAR(opt.cost, 3.6);
      }{
        ParameterizeForNode<double> param(&problem, &acc, NodeIndex(126));

        param.setState(&problem, StateIndex(-353));
        CHECK_NEAR(param.minimizeTop().cost, 3.6);
        param.setState(&problem, StateIndex(90));
        CHECK_NEAR(param.minimizeTop().cost, 3.7);
        param.setState(&problem, StateIndex(-353));
        CHECK_NEAR(param.minimizeTop().cost, 3.6);
      }{
        ParameterizeForNode<double> param(&problem, &acc, NodeIndex(-43));
        param.setState(&problem, StateIndex(1000));
        CHECK_NEAR(param.minimizeTop().cost, 3.6);
        param.setState(&problem, StateIndex(1001));
        CHECK_NEAR(param.minimizeTop().cost, 3.7);
        param.setState(&problem, StateIndex(1000));
        CHECK_NEAR(param.minimizeTop().cost, 3.6);
      }


      acc.changeEnabledStates(&problem, {
          NodeStateEnabledState(126, -353, false)
      });

      {
        auto opt = acc.solve();
        CHECK(opt.node2stateMap.at(NodeIndex(126)) == StateIndex(90));
        CHECK(opt.node2stateMap.at(NodeIndex(-43)) == StateIndex(1001));
        CHECK_NEAR(opt.cost, 3.7);
      }

      acc.changeEnabledStates(&problem, {
          NodeStateEnabledState(126, -353, true),
          NodeStateEnabledState(-43, 1000, false),
      });


      {
        auto opt = acc.solve();
        CHECK(opt.node2stateMap.at(NodeIndex(126)) == StateIndex(90));
        CHECK(opt.node2stateMap.at(NodeIndex(-43)) == StateIndex(1001));
        CHECK_NEAR(opt.cost, 3.7);

        LOG("\n\n\nSOLVE PARAMETERIZED PROBLEMATIC");
        ParameterizeForNode<double> param(&problem, &acc, NodeIndex(-43));
        DOUT(param.minimizeTop().cost);
        CHECK_NEAR(param.minimizeTop().cost, 3.7);

        param.setState(&problem, StateIndex(1000));
        CHECK_NEAR(param.minimizeTop().cost, 3.6);
      }


      acc.changeEnabledStates(&problem, {
          NodeStateEnabledState(-43, 1000, true),
      });

      {
        auto opt = acc.solve();
        CHECK(opt.node2stateMap.at(NodeIndex(126)) == StateIndex(-353));
        CHECK(opt.node2stateMap.at(NodeIndex(-43)) == StateIndex(1000));
        CHECK_NEAR(opt.cost, 3.6);
      }

    }{
      std::vector<double> positions{0.3, 0.9, 4.25, 4.34, 5.25, 13.4, 14.9};
      TestProblem problem(positions,
        {
          {NodeIndex(0), {nodeInds({1}), {}}},
          {NodeIndex(1), {nodeInds({}), {}}}
        });
      
      auto sol = optimize(&problem);
      CHECK(sol.cost == 0.0);
      CHECK(positions[sol.node2stateMap[NodeIndex(0)].value] == 4.25);
      CHECK(positions[sol.node2stateMap[NodeIndex(1)].value] == 5.25);
    }{
      std::vector<double> positions{234.324, 234.34, 7.5/*2*/, 2.34, 6.323, 8.5/*5*/, 3.3, 9.5/*7*/, 77.3};
      TestProblem problem(positions, {
          {NodeIndex(0), {nodeInds({5, 3, 1}), stateInds({})}},
          {NodeIndex(1), {nodeInds({2}), stateInds({})}},
          {NodeIndex(2), {nodeInds({}), stateInds({})}},
          {NodeIndex(3), {nodeInds({4, 7}), stateInds({4, 5, 6, 2})}},
          {NodeIndex(4), {nodeInds({}), stateInds({})}},
          {NodeIndex(5), {nodeInds({80}), stateInds({})}},
          {NodeIndex(7), {nodeInds({}), stateInds({})}},
          {NodeIndex(80), {nodeInds({}), stateInds({})}}
        });
      
      {
        auto sol = optimize(&problem);
        checkSol(sol);
      }
      {
        using namespace ccopt::TreeStateOptimizerImpl;
        TreeStateOptimizationAccumulation<double> acc(&problem, TreeStateOptimizerSettings());

        {
          nlohmann::json data = acc;

          TreeStateOptimizationAccumulation<double> acc2 = data;
          {
            auto sol = acc2.solve();
            checkSol(sol);
          }{
            auto sol = acc2.solveWithAcc(&problem, ccopt::TreeStateOptimizerImpl::CandAcc<double>());
            checkSol(sol);
          }
        }{

          ParameterizeForNode<double> param(&problem, &acc, NodeIndex(80));
          CHECK(param.totalUpdatedStateCount() == 0);
          CHECK(param.minimizeTop().cost == 0.0);

          param.setState(&problem, StateIndex(7));
          CHECK(param.minimizeTop().cost == 0.0);
          CHECK(0 < param.totalUpdatedStateCount());

          // Set it again: This time, nothing gets updated.
          param.setState(&problem, StateIndex(7));
          CHECK(param.minimizeTop().cost == 0.0);
          CHECK(0 == param.totalUpdatedStateCount());

          // Set another state.
          {
            param.setState(&problem, StateIndex(8));
            auto c = param.minimizeTop().cost;
            CHECK(0.0 < c);
            CHECK(0 < param.totalUpdatedStateCount());

            param.setState(&problem, StateIndex(8));
            CHECK(c == param.minimizeTop().cost);
            CHECK(0 == param.totalUpdatedStateCount());
          }


          param.setState(&problem, StateIndex(7));
          CHECK(0.0 == param.minimizeTop().cost);
          CHECK(0 < param.totalUpdatedStateCount());

          param.setState(&problem, StateIndex(7));
          CHECK(0.0 == param.minimizeTop().cost);
          CHECK(0 == param.totalUpdatedStateCount());
        }
      }
    }{
      auto root = initNode(68, {{StateIndex(316), 0.85, 0.02}, {StateIndex(335), 0.59, 0.99}});
      attachChild(root, 68, initNode(747, {{StateIndex(773), 0.10, 0.98}, {StateIndex(483), 0.78, 0.90}, {StateIndex(180), 0.25, 0.98}, {StateIndex(685), 0.71, 0.86}, {StateIndex(116), 0.86, 0.34}}));
      attachChild(root, 68, initNode(771, {{StateIndex(38), 0.40, 0.74}, {StateIndex(521), 0.66, 0.25}}));
      attachChild(root, 771, initNode(602, {{StateIndex(491), 0.48, 0.33}}));
      attachChild(root, 747, initNode(43, {{StateIndex(63), 0.30, 0.28}}));
      attachChild(root, 43, initNode(744, {{StateIndex(36), 0.86, 0.09}}));
      attachChild(root, 747, initNode(336, {{StateIndex(115), 0.94, 0.91}, {StateIndex(591), 0.43, 0.98}, {StateIndex(744), 0.24, 0.92}}));
      attachChild(root, 771, initNode(340, {{StateIndex(14), 0.46, 0.86}, {StateIndex(443), 0.40, 0.50}}));
      attachChild(root, 771, initNode(92, {{StateIndex(979), 0.38, 0.70}, {StateIndex(796), 0.61, 0.86}, {StateIndex(561), 0.98, 0.96}, {StateIndex(897), 0.18, 0.34}}));
      attachChild(root, 336, initNode(506, {{StateIndex(975), 0.80, 0.73}, {StateIndex(579), 0.10, 0.95}, {StateIndex(83), 0.65, 0.75}, {StateIndex(817), 0.87, 0.81}, {StateIndex(118), 0.35, 0.11}, {StateIndex(492), 0.05, 0.43}}));
      attachChild(root, 336, initNode(742, {{StateIndex(214), 0.51, 0.70}, {StateIndex(677), 0.64, 0.69}}));
      attachChild(root, 336, initNode(30, {{StateIndex(146), 0.68, 0.55}, {StateIndex(496), 0.01, 0.00}, {StateIndex(560), 0.83, 0.99}, {StateIndex(512), 0.98, 0.08}, {StateIndex(847), 0.78, 0.20}}));
      attachChild(root, 43, initNode(384, {{StateIndex(664), 0.07, 0.88}, {StateIndex(93), 0.83, 0.72}}));
      attachChild(root, 506, initNode(293, {{StateIndex(214), 0.09, 0.85}, {StateIndex(291), 0.91, 0.35}, {StateIndex(728), 0.68, 0.67}}));
      attachChild(root, 602, initNode(661, {{StateIndex(480), 0.31, 0.42}, {StateIndex(571), 0.25, 0.56}, {StateIndex(285), 0.46, 0.04}, {StateIndex(648), 0.81, 0.80}, {StateIndex(527), 0.81, 0.51}}));
      attachChild(root, 43, initNode(848, {{StateIndex(37), 0.02, 0.57}, {StateIndex(150), 0.83, 0.06}, {StateIndex(281), 0.84, 0.96}, {StateIndex(441), 0.04, 0.44}, {StateIndex(247), 0.47, 0.40}, {StateIndex(127), 0.21, 0.07}}));
      attachChild(root, 43, initNode(750, {{StateIndex(73), 0.75, 0.60}, {StateIndex(224), 0.98, 0.53}, {StateIndex(882), 0.73, 0.07}, {StateIndex(420), 0.99, 0.27}}));
      attachChild(root, 43, initNode(854, {{StateIndex(413), 0.82, 0.71}, {StateIndex(855), 0.34, 0.68}, {StateIndex(820), 0.44, 0.57}, {StateIndex(661), 0.10, 0.47}, {StateIndex(645), 0.20, 0.52}, {StateIndex(631), 0.82, 0.94}}));
      attachChild(root, 30, initNode(46, {{StateIndex(646), 0.44, 0.93}, {StateIndex(792), 0.20, 0.75}, {StateIndex(386), 0.10, 0.26}, {StateIndex(115), 0.27, 0.44}, {StateIndex(232), 0.08, 0.67}, {StateIndex(228), 0.02, 0.23}, {StateIndex(901), 0.57, 0.37}}));
      attachChild(root, 848, initNode(531, {{StateIndex(598), 0.18, 0.81}, {StateIndex(524), 0.38, 0.96}}));
      attachChild(root, 92, initNode(994, {{StateIndex(100), 0.99, 0.32}, {StateIndex(441), 0.36, 0.70}, {StateIndex(761), 0.20, 0.00}}));
      attachChild(root, 506, initNode(282, {{StateIndex(590), 0.42, 0.59}, {StateIndex(365), 0.85, 0.18}, {StateIndex(204), 0.49, 0.37}, {StateIndex(740), 0.26, 0.77}, {StateIndex(634), 0.82, 0.37}, {StateIndex(668), 0.01, 0.17}, {StateIndex(845), 0.30, 0.46}, {StateIndex(616), 0.78, 0.99}}));
      attachChild(root, 293, initNode(837, {{StateIndex(236), 0.40, 0.44}, {StateIndex(824), 0.25, 0.85}, {StateIndex(611), 0.59, 0.42}, {StateIndex(856), 0.88, 0.17}, {StateIndex(631), 0.12, 0.57}, {StateIndex(723), 0.36, 0.05}}));
      attachChild(root, 994, initNode(678, {{StateIndex(778), 0.02, 0.48}, {StateIndex(382), 0.67, 0.03}}));
      attachChild(root, 46, initNode(674, {{StateIndex(850), 0.21, 0.61}, {StateIndex(786), 0.28, 0.99}, {StateIndex(807), 0.06, 0.66}, {StateIndex(681), 0.93, 0.30}, {StateIndex(814), 0.84, 0.26}}));
      attachChild(root, 336, initNode(140, {{StateIndex(664), 0.65, 0.75}, {StateIndex(861), 0.41, 0.47}, {StateIndex(923), 0.93, 0.23}, {StateIndex(5), 0.32, 0.20}}));
      attachChild(root, 68, initNode(503, {{StateIndex(431), 0.59, 0.40}, {StateIndex(643), 0.39, 0.19}, {StateIndex(214), 0.49, 0.10}, {StateIndex(512), 0.86, 0.30}, {StateIndex(90), 0.52, 0.68}}));
      attachChild(root, 744, initNode(625, {{StateIndex(550), 0.93, 0.56}, {StateIndex(671), 0.27, 0.20}, {StateIndex(473), 0.12, 0.92}, {StateIndex(456), 0.08, 0.81}, {StateIndex(880), 0.90, 0.21}, {StateIndex(142), 0.79, 0.73}, {StateIndex(992), 0.60, 0.43}, {StateIndex(712), 0.27, 0.22}}));
      attachChild(root, 340, initNode(402, {{StateIndex(591), 0.25, 0.93}, {StateIndex(756), 0.26, 0.26}, {StateIndex(200), 0.96, 0.85}}));
      attachChild(root, 384, initNode(829, {{StateIndex(923), 0.27, 0.02}, {StateIndex(525), 0.26, 0.95}, {StateIndex(568), 0.95, 0.17}, {StateIndex(470), 0.49, 0.69}, {StateIndex(956), 0.58, 0.59}}));
    }{
      auto root = initNode(430, {{StateIndex(326), 0.80, 0.64}, {StateIndex(615), 0.00, 0.99}, {StateIndex(255), 0.62, 0.84}, {StateIndex(183), 0.55, 0.58}, {StateIndex(219), 0.13, 0.53}, {StateIndex(982), 0.68, 0.54}, {StateIndex(705), 0.41, 0.33}});
      attachChild(root, 430, initNode(600, {{StateIndex(726), 0.79, 0.46}, {StateIndex(522), 0.33, 0.87}, {StateIndex(716), 0.64, 0.25}, {StateIndex(896), 0.53, 0.89}, {StateIndex(588), 0.01, 0.06}}));
      attachChild(root, 600, initNode(775, {{StateIndex(539), 0.45, 0.04}}));
      attachChild(root, 600, initNode(866, {{StateIndex(991), 0.74, 0.78}, {StateIndex(152), 0.13, 0.66}, {StateIndex(487), 0.75, 0.68}, {StateIndex(878), 0.56, 0.40}, {StateIndex(594), 0.37, 0.60}, {StateIndex(570), 0.08, 0.82}}));
      attachChild(root, 775, initNode(955, {{StateIndex(610), 0.47, 0.12}, {StateIndex(495), 0.05, 0.52}, {StateIndex(224), 0.19, 0.20}, {StateIndex(735), 0.70, 0.73}, {StateIndex(923), 0.82, 0.13}}));
      attachChild(root, 955, initNode(700, {{StateIndex(900), 0.19, 0.71}, {StateIndex(843), 0.51, 0.28}, {StateIndex(995), 0.19, 0.55}, {StateIndex(597), 0.42, 0.61}}));
      attachChild(root, 775, initNode(807, {{StateIndex(545), 0.03, 0.36}, {StateIndex(123), 0.76, 0.01}, {StateIndex(992), 0.36, 0.27}, {StateIndex(403), 0.30, 0.52}, {StateIndex(864), 0.62, 0.15}, {StateIndex(655), 0.85, 0.83}, {StateIndex(696), 0.97, 0.26}}));
      attachChild(root, 775, initNode(506, {{StateIndex(888), 0.92, 0.07}, {StateIndex(510), 0.18, 0.63}, {StateIndex(53), 0.77, 0.36}, {StateIndex(456), 0.81, 0.68}, {StateIndex(790), 0.85, 0.20}, {StateIndex(905), 0.08, 0.57}, {StateIndex(932), 0.16, 0.13}, {StateIndex(762), 0.89, 0.90}}));
      attachChild(root, 955, initNode(738, {{StateIndex(839), 0.31, 0.38}, {StateIndex(658), 0.39, 0.07}}));
      attachChild(root, 430, initNode(396, {{StateIndex(482), 0.69, 0.84}, {StateIndex(732), 0.47, 0.10}, {StateIndex(814), 0.23, 0.14}, {StateIndex(222), 0.82, 0.87}, {StateIndex(998), 0.19, 0.04}, {StateIndex(892), 0.45, 0.67}}));
      attachChild(root, 600, initNode(235, {{StateIndex(788), 0.67, 0.47}, {StateIndex(507), 0.77, 0.03}, {StateIndex(539), 0.73, 0.90}, {StateIndex(952), 0.54, 0.58}, {StateIndex(625), 0.78, 0.75}, {StateIndex(50), 0.11, 0.08}, {StateIndex(379), 0.95, 0.51}}));
      attachChild(root, 600, initNode(91, {{StateIndex(73), 0.39, 0.46}, {StateIndex(43), 0.00, 0.24}, {StateIndex(267), 0.64, 0.17}, {StateIndex(222), 0.32, 0.51}, {StateIndex(526), 0.16, 0.72}, {StateIndex(309), 0.66, 0.04}, {StateIndex(731), 0.24, 0.06}}));
      attachChild(root, 738, initNode(956, {{StateIndex(57), 0.46, 0.80}, {StateIndex(669), 0.74, 0.07}, {StateIndex(755), 0.37, 0.55}, {StateIndex(341), 0.67, 0.04}, {StateIndex(266), 0.70, 0.40}, {StateIndex(677), 0.92, 0.71}, {StateIndex(224), 0.44, 0.94}}));
      attachChild(root, 866, initNode(557, {{StateIndex(23), 0.19, 0.63}, {StateIndex(52), 0.36, 0.60}, {StateIndex(656), 0.27, 0.90}, {StateIndex(192), 0.81, 0.35}}));
      attachChild(root, 738, initNode(362, {{StateIndex(436), 0.85, 0.05}}));
      attachChild(root, 430, initNode(857, {{StateIndex(663), 0.15, 0.07}, {StateIndex(215), 0.42, 0.73}}));
      attachChild(root, 506, initNode(704, {{StateIndex(387), 0.56, 0.98}, {StateIndex(206), 0.70, 0.62}, {StateIndex(488), 0.65, 0.07}, {StateIndex(51), 0.38, 0.23}}));
      attachChild(root, 866, initNode(969, {{StateIndex(404), 0.40, 0.16}, {StateIndex(989), 0.94, 0.59}, {StateIndex(978), 0.70, 0.58}}));
      attachChild(root, 969, initNode(875, {{StateIndex(489), 0.60, 0.17}, {StateIndex(891), 0.67, 0.14}, {StateIndex(785), 0.45, 0.88}, {StateIndex(520), 0.14, 0.38}, {StateIndex(944), 0.42, 0.18}}));
      attachChild(root, 738, initNode(424, {{StateIndex(514), 0.74, 0.85}, {StateIndex(519), 0.62, 0.21}, {StateIndex(264), 0.40, 0.38}, {StateIndex(880), 0.89, 0.35}, {StateIndex(885), 0.43, 0.85}, {StateIndex(343), 0.47, 0.73}, {StateIndex(276), 0.45, 0.95}}));
      attachChild(root, 775, initNode(996, {{StateIndex(762), 0.26, 0.15}, {StateIndex(704), 0.67, 0.70}, {StateIndex(231), 0.62, 0.32}, {StateIndex(469), 0.42, 0.43}}));
      attachChild(root, 955, initNode(665, {{StateIndex(256), 0.20, 0.03}, {StateIndex(295), 0.58, 0.30}, {StateIndex(100), 0.68, 0.37}, {StateIndex(876), 0.80, 0.81}, {StateIndex(962), 0.85, 0.81}, {StateIndex(396), 0.50, 0.42}}));
      attachChild(root, 704, initNode(432, {{StateIndex(129), 0.59, 0.60}}));
      attachChild(root, 557, initNode(426, {{StateIndex(106), 0.25, 0.99}, {StateIndex(48), 0.24, 0.74}}));
      attachChild(root, 875, initNode(386, {{StateIndex(507), 0.67, 0.34}, {StateIndex(992), 0.01, 0.62}, {StateIndex(828), 0.22, 0.36}, {StateIndex(822), 0.26, 0.54}, {StateIndex(848), 0.45, 0.36}, {StateIndex(876), 0.35, 0.91}, {StateIndex(629), 0.52, 0.19}}));
      attachChild(root, 600, initNode(582, {{StateIndex(361), 0.83, 0.94}, {StateIndex(928), 0.37, 0.13}, {StateIndex(494), 0.73, 0.80}}));
      attachChild(root, 969, initNode(318, {{StateIndex(210), 0.90, 0.58}, {StateIndex(121), 0.22, 0.52}, {StateIndex(835), 0.24, 0.40}}));
      attachChild(root, 362, initNode(38, {{StateIndex(706), 0.56, 0.54}}));
      attachChild(root, 996, initNode(585, {{StateIndex(835), 0.56, 0.35}, {StateIndex(207), 0.25, 0.10}, {StateIndex(440), 0.84, 0.18}, {StateIndex(285), 0.86, 0.14}, {StateIndex(746), 0.89, 0.57}, {StateIndex(99), 0.70, 0.04}}));
      attachChild(root, 506, initNode(833, {{StateIndex(209), 0.37, 0.20}}));
    }
  }
}



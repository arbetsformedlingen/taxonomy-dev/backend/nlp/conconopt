#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <functional>

namespace ccopt {

#define CRASH(msg) {std::cerr << msg << "\n"; abort();}
#define CHECK(X) if(!(X)) {CRASH("CHECK FAILED: " << #X << " in file " << __FILE__ << " on line " << __LINE__);}
#define DOUT(X) {std::cout << #X " = " << (X) << std::endl;}
#define LOG(X) {std::cout << X << std::endl;}

template <typename T>
class Delayed {
public:
  Delayed() {}
  Delayed(std::function<T()> f) : _f(f) {}

  T get() {
    CHECK(bool(_f));
    if (!_evaled) {
      _value = _f();
      _evaled = true;
    }
    return _value;
  }
private:
  bool _evaled = false;
  T _value = T();
  std::function<T()> _f;
};

}



#endif

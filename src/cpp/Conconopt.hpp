#ifndef __CONCONOPT_HPP__
#define __CONCONOPT_HPP__

#include <limits>
#include <nlohmann/json.hpp>
#include <string>
#include <memory>
#include "SkosTerminology.hpp"
#include "TerminologyTree.hpp"
#include "TreeStateOptimizer.hpp"
#include "utils.hpp"
#include "GaussianCost.hpp"
#include <random>
#include <limits>
#include <string>
#include <set>

namespace ccopt {
  class DiscreteWeightMapping { // x = base^(y/resolution) <=> e^ln(x) = e^(ln(base)*(y/resolution)) <=> y = resolution*(ln(x)/ln(base))
  public:
    DiscreteWeightMapping() {}
    DiscreteWeightMapping(int base, int resolution);
    static DiscreteWeightMapping centMapping();
    int map(double x) const;
    double unmap(int y) const;
    double normalize(double x) const {return unmap(map(x));}

    std::string encodeValue(double v) const;
    double decodeString(const std::string& x) const;
  private:
    double _lnBase = 0.0;
    int _resolution = 0;
    int _base = 0;
  };

  class ConconoptSetup;
  
  class IConconoptTask {
  public:
    typedef std::shared_ptr<IConconoptTask> Ptr;

    virtual std::string taskName() const = 0;
    virtual void perform(ConconoptSetup* setup) = 0;
    virtual void configure(ConconoptSetup* setup) {}

    virtual ~IConconoptTask() {}
  };

  class IConconoptCost {
  public:
    typedef std::shared_ptr<IConconoptCost> Ptr;
      
    virtual double getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const {return 0.0;}
    virtual double getTransitionCost(
        NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
        StateIndex childState, StateIndex parentState) const {return 0.0;}
    virtual std::string toString() const {return "IConconoptCost";}
    virtual nlohmann::json analyze() const {return nullptr;}
    virtual ~IConconoptCost() {}
  };

  struct WeightedConconoptCost {
    WeightedConconoptCost() : weight(0.0) {}
    WeightedConconoptCost(double w, const IConconoptCost::Ptr& c)
      : weight(w), cost(c) {}

    double weight = 1.0;
    IConconoptCost::Ptr cost;
  };

  class ConconoptProblem : public ITreeStateOptimizationProblem<GaussianCost> {
  public:
    ConconoptProblem(
        const TerminologyTreeLookup::Ptr& srcTreeLookup,
        const TerminologyTreeLookup::Ptr& dstTreeLookup,
        const std::map<std::string, WeightedConconoptCost>& costs)
    : _srcTreeLookup(srcTreeLookup),
      _dstTreeLookup(dstTreeLookup),
      _costs(costs) {
      for (const auto& x: dstTreeLookup->nodeData()) {
        _allDstInds.push_back(StateIndex(x.index));
      }
      for (const auto& x: srcTreeLookup->nodeData()) {
        _children[NodeIndex(x.index)] = nodeInds(x.children);
      }

    }

    NodeIndex getRootNode() const override {
      return NodeIndex(_srcTreeLookup->root());
    }

    const std::vector<NodeIndex>& getChildren(NodeIndex nodeIndex) const override {
      return _children.at(nodeIndex);
    }

    const std::vector<StateIndex>& getStates(NodeIndex nodeIndex) const override {
      return _allDstInds;
    }

    GaussianCost getStateCost(NodeIndex nodeIndex, StateIndex stateIndex) const override;
    GaussianCost getTransitionCost(
        NodeIndex childNodeIndex, NodeIndex parentNodeIndex,
        StateIndex childState, StateIndex parentState) const override;

    void reportAccumulationProgress(int processedCount, int totalCount) override;
  private:
    TerminologyTreeLookup::Ptr _srcTreeLookup, _dstTreeLookup;
    std::vector<StateIndex> _allDstInds;
    std::unordered_map<NodeIndex, std::vector<NodeIndex>> _children;
    std::map<std::string, WeightedConconoptCost> _costs;
  };

  class ParameterSweep : public IConconoptTask {
  public:
    std::string taskName() const {return "ParameterSweep";}
    ParameterSweep() {}
    ParameterSweep(const nlohmann::json& src);

    void perform(ConconoptSetup* setup);

    std::shared_ptr<ConconoptProblem> problemForIndex(ConconoptSetup* setup, int i);
    double valueForIndex(int i) const;
    std::string filenameForIndex(int i) const;

    std::map<std::string, WeightedConconoptCost> tweakObjectiveFunctionTermsForIndex(
        const std::map<std::string, WeightedConconoptCost>& terms,
        int i) const;

    int count() const {return _count;}
  private:
    DiscreteWeightMapping _weightMapping;
    std::string _termKey;
    int _count = 0;
    double _minValue = 0.0;
    double _maxValue = 0.0;
    std::string _outputPrefix;

  };

  struct ConceptPair {
    typedef std::string Tag;
    ConceptPair() {}
    ConceptPair(
        const SkosKey& s, const SkosKey& d,
        const std::set<std::string>& tgs, int n = 1)
      : src(s), dst(d), tags(tgs), count(n) {}

    SkosKey src, dst;
    int count = 1;
    std::set<std::string> tags;

    ConceptPair combine(const ConceptPair& other) const;

    std::pair<SkosKey, SkosKey> key() const {
      return {src, dst};
    }
  };

  struct ConceptMapping {
    double cost = std::numeric_limits<double>::infinity();

    std::vector<ConceptPair> srcDstPairs;

    ConceptMapping compact() const;
    static ConceptMapping merge(const std::vector<ConceptMapping>& other);
  };



  struct ConconoptSetup {
    std::default_random_engine rng;
    double constraintPenalty = 100000.0;
    std::string lang = "sv";
    std::string accumulatedCostsFilename;
    TerminologySetup src, dst;
    std::map<std::string, Delayed<WeightedConconoptCost>> objectiveFunctionTerms;
    std::map<std::string, IConconoptTask::Ptr> taskMap;
    int sampleCount = 0;
    double sigma = 0.0;

    ConconoptSetup();
    TreeStateOptimizationAccumulation<GaussianCost> accumulate(
        const std::shared_ptr<ConconoptProblem>& problem) const;
    std::map<std::string, WeightedConconoptCost> realizeTerms();
    std::shared_ptr<ConconoptProblem> initializeOptimizationProblem(
        const std::map<std::string, WeightedConconoptCost>& terms);
    std::shared_ptr<ConconoptProblem> initializeOptimizationProblem();

    template <typename Task>
    std::shared_ptr<Task> getTaskOfType(const std::string& k) {
      auto f = taskMap.find(k);
      if (f == taskMap.end()) {
        return nullptr;
      }
      if (Task().taskName() != f->second->taskName()) {
        return nullptr;
      }
      return std::reinterpret_pointer_cast<Task>(f->second);
    }


    static ConconoptSetup loadFromConfig(const std::string& filename);

    ConceptMapping conceptMappingFromSolution(
        const ccopt::TreeStateOptimizationSolution<GaussianCost>& sol,
        const std::set<ConceptPair::Tag>& tags) const;

    void performTask(const std::string& taskKey);

    void writeConceptMappingCSV(const std::string& filename, const ConceptMapping& cm) const;
  };

  void testConconopt();
}

#endif

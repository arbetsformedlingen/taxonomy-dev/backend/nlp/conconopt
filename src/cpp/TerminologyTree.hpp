#ifndef __TERMINOLOGY_TREE_HPP__
#define __TERMINOLOGY_TREE_HPP__

#include <memory>
#include <vector>
#include "SkosTerminology.hpp"
#include <nlohmann/json.hpp>

namespace ccopt {
  class TerminologyTreeNode {
  public:
    typedef std::shared_ptr<TerminologyTreeNode> Ptr;

    static KeyType* rootKeyType();
    static KeyType* childNodeKeyType();
    static SkosKey childNodeKey(const std::string& id);
    static SkosKey rootKey();

    TerminologyTreeNode() {}
      
    TerminologyTreeNode(const SkosKey& k, const std::vector<TerminologyTreeNode::Ptr>& children)
      : _key(k), _children(children) {}

    const SkosKey& key() const {return _key;}
    const std::vector<TerminologyTreeNode::Ptr>& children() const {return _children;}
      
    virtual ~TerminologyTreeNode() {}
  private:
    SkosKey _key;
    std::vector<TerminologyTreeNode::Ptr> _children;
  };
  
  class TerminologyTreeLookup {
  public:
    TerminologyTreeLookup(const TerminologyTreeNode::Ptr tree);

    typedef std::shared_ptr<TerminologyTreeLookup> Ptr;
      
    static const int UNDEFINED_INDEX = -1;
    struct NodeData {
      SkosKey key;
      int index;
      int parent;
      int depth;
      std::vector<int> children;
    };
      


    struct PathSummary {
      int minimumCommonNode = UNDEFINED_INDEX;
      int ascentSteps = 0;
      int descentSteps = 0;

      bool hasIndirectChild() const {
        return ascentSteps == 0 && descentSteps > 0;
      }

      bool hasIndirectParent() const {
        return ascentSteps > 0 && descentSteps == 0;
      }

      int length() const {
        return ascentSteps == 0 && descentSteps == 0;
      }

      bool areIdentitical() const {
        return length() == 0;
      }
    };

    PathSummary summarizePath(int src, int dst) const;

    int root() const {return _root;}

    const NodeData &rootData() const {
      return get(_root);
    }

    const NodeData &get(const SkosKey& k) const;
    const NodeData &get(int i) const;

    const std::vector<NodeData>& nodeData() const {return _data;}

    size_t size() const {return _data.size();}
  private:
    int _root = -1;
    std::vector<NodeData> _data;
    std::map<SkosKey, int> _node2index;

    int initializeNodeData(int depth, int parent, const TerminologyTreeNode::Ptr& tree);
  };

  struct TerminologySetup {
    bool addRoot = true;
    bool addUndefinedChildNodes = false;

    SkosTerminology terminology;
    TerminologyTreeLookup::Ptr treeLookup;

    TerminologyTreeNode::Ptr buildTree() const;
    void loadFromJson(const nlohmann::json& src);

    nlohmann::json analyze() const;
  };

  TerminologyTreeLookup::Ptr terminologyTreeLookup(const TerminologySetup& s);
}

#endif

/*
 * ReduceTree.h
 *
 *  Created on: Nov 10, 2022
 *      Author: jonas
 */

#ifndef REDUCETREE_HPP_
#define REDUCETREE_HPP_

#include <vector>
#include <iosfwd>
#include <optional>
#include "utils.hpp"

namespace ccopt {

template <typename T, typename F>
class ReduceTree {
public:
  ReduceTree() {}
  ReduceTree(const std::vector<std::optional<T>>& init, const F& f = F()) : _f(f), _size(init.size()) {
    int size = init.size();
    CHECK(0 < size);
    int left = 0;
    int right = 0;
    while (right + 1 - left < size) {
      left = leftChild(left);
      right = rightChild(right);
    }
    _dataLower = left;
    _dataUpper = right + 1;
    _data.resize(_dataUpper);
    for (int i = 0; i < size; i++) {
      _data[_dataLower + i] = init[i];
    }
    for (int i = 0; i < _dataUpper; i++) {
      recomputeNode(_dataUpper - 1 - i);
    }
  }

  void set(int i, const std::optional<T>& x) {
    CHECK(0 <= i);
    CHECK(i < _size);
    int internalIndex = _dataLower + i;
    _data[internalIndex] = x;
    recomputeTowardsRoot(internalIndex);
  }

  const std::optional<T>& root() const {
    CHECK(!empty());
    return _data[0];
  }

  static int leftChild(int i) {return 2*i + 1;}
  static int rightChild(int i) {return 2*i + 2;}
  static int parent(int i) {
    return i <= 0? -1 : (i - 1)/2;
  }

  bool empty() const {return _size == 0;}
  int size() const {return _size;}

  virtual ~ReduceTree() {}
private:
  F _f;
  int _dataLower = 0;
  int _dataUpper = 0;
  int _size = 0;
  std::vector<std::optional<T>> _data;

  std::optional<T> computeCell(const std::optional<T>& a, const std::optional<T>& b) const {
    if (!a) {
      return b;
    } else if (!b) {
      return a;
    }
    return _f(a.value(), b.value());
  }

  void recomputeNode(int i) {
    if (i < _dataLower) {
      _data[i] = computeCell(_data[leftChild(i)], _data[rightChild(i)]);
    }
  }

  void recomputeTowardsRoot(int i) {
    while (i != -1) {
      recomputeNode(i);
      i = parent(i);
    }
  }
};

void testReduceTree();

}


#endif /* REDUCETREE_HPP_ */

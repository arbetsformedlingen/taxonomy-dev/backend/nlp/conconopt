/*
 * ReduceTree.cpp
 *
 *  Created on: Nov 10, 2022
 *      Author: jonas
 */

#include <iostream>
#include "ReduceTree.hpp"

namespace ccopt {

namespace {
  struct Min {
    int operator()(int a, int b) const {
      return std::min(a, b);
    }
  };

}

void testReduceTree() {
  std::vector<int> data{
    3706, 8582, 5721, 1619, 8010, 1616, 7213, 5261, 1256, 3261, 8445, 364, 525, 9787, 2159, 355, 381, 9859, 4073, 2721, 654, 8299, 2089, 5729, 7976, 642, 5770, 3123, 5081, 7609, 4184, 9397, 7449, 5121, 8430, 2282, 809, 3912, 6, 4721, 5884, 4495, 6913, 9864, 4667, 3900, 27, 7883, 9831, 2846, 3904, 4962, 333, 9813, 6845, 9688, 6502, 3698, 7773, 6591, 5778, 8036, 4881, 111, 6919, 6130, 9535, 4748, 8205, 2826, 1337, 6275, 4709, 4287, 8617, 7553, 8201, 9253, 6336, 1147, 196, 9089, 8811, 5209, 2455, 8951, 1558, 6694, 9870, 9341, 6016, 127, 3137, 4458, 8538, 4108, 4944, 6380, 9729, 6623
  };

  int minValue = 1000000;
  std::vector<std::optional<int>> slice;
  for (int i = 0; i < data.size(); i++) {
    int x = data[i];
    minValue = std::min(minValue, x);
    slice.push_back(x);
    ReduceTree<int, Min> tree(slice);
    CHECK(tree.root().value() == minValue);
    for (int j = 0; j < slice.size(); j++) {
      tree.set(j, -j);
      CHECK(tree.root().value() == -j);
      tree.set(j, slice[j]);
      CHECK(tree.root().value() == minValue);
    }
  }

}

}


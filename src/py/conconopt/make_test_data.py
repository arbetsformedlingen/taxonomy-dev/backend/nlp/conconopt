import random

max_node_index = 1000
max_state_index = 1000
node_count = 30
max_state_count = 8

node_inds = list(range(max_node_index+1))
random.shuffle(node_inds)

def random_state(index):
    cost = random.random()
    position = random.random()
    return "{{StateIndex({:d}), {:.2f}, {:.2f}}}".format(index, cost, position)

for counter, i in enumerate(range(node_count)):
    index = node_inds[i]
    state_count = random.randint(1, max_state_count)
    state_inds = list(range(max_state_index))
    random.shuffle(state_inds)
    state_inds = state_inds[0:state_count]
    states = "{" + ", ".join([random_state(k) for k in state_inds]) + "}"
    nodeexpr = "initNode({:d}, {:s})".format(index, states)
    if counter == 0:
        print("auto root = " + nodeexpr + ";")
    else:
        parent = random.sample(node_inds[0:counter], 1)[0]
        print("attachChild(root, {:d}, {:s});".format(parent, nodeexpr))
        


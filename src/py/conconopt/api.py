dev = False
## Code

import rdflib
import json
from conconopt.json_utils import read_json, write_json
from sentence_transformers import SentenceTransformer

def strings_from_labels(labels, language):
    return [label["@value"] for label in labels if label["@language"] == language]

def encode_strings(sbert_model, strings):
    assert(isinstance(strings, list))
    for x in strings:
        assert(isinstance(x, str))

    matrix = sbert_model.encode(strings)
    return {k:v for (k, v) in zip(strings, matrix.tolist())}
    
label_ks = {'http://www.w3.org/2004/02/skos/core#prefLabel', 'http://www.w3.org/2004/02/skos/core#altLabel'}

def visit_labels(data, visitor_fn):
    if isinstance(data, list):
        for x in data:
            visit_labels(x, visitor_fn)
    elif isinstance(data, dict):
        for (k, v) in data.items():
            if k in label_ks:
                for x in v:
                    visitor_fn(x)
            else:
                visit_labels(v, visitor_fn)

class ConconoptApi:
    def __init__(self):
        self.sbert_model_path = 'KBLab/sentence-bert-swedish-cased'
        self.language = "sv"
    
    def sbert_embed_labels(self, input_jsonld_files, output_json_file):
        input_terminologies = [read_json(filename) for filename in input_jsonld_files]
        labels = []
        def visitor(x):
            labels.append(x)
        visit_labels(input_terminologies, visitor)
        label_strings = strings_from_labels(labels, self.language)
        print("Loaded {:d} labels".format(len(label_strings)))
        sbert_model = SentenceTransformer(self.sbert_model_path)
        output = encode_strings(sbert_model, label_strings)
        write_json(output_json_file, output)
        print("Wrote embedded labels to {:s}".format(output_json_file))
        
    def convert_rdf(self, src_filename, src_fmt, dst_filename, dst_fmt):
        """Convert between RDF formats"""
        g = rdflib.Graph()
        g.parse(src_filename, format=src_fmt)
        g.serialize(destination=dst_filename, format=dst_fmt)

    def slice_json(self, src_filename, lower, upper, dst_filename):
        data = read_json(src_filename)
        write_json(dst_filename, data[lower:upper])
        
if dev:
    data = read_json("../../../data/sni.jsonld")
    labels = []
    visit_labels(data, lambda x: labels.append(x))
    swedish = strings_from_labels(labels, "sv")
    model = SentenceTransformer(ConconoptApi().sbert_model_path)
    result = encode_strings(model, swedish[0:10])
    print("Done")
        
## 

if dev:
    path = "../../data/occupation.ttl"
    
    # https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph
    
    g = rdflib.Graph()
    g.parse(path)


## Activate dev

dev = True

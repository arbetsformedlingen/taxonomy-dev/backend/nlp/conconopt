import json
import os
import math
import matplotlib.pyplot as plt

from conconopt.json_utils import read_json, write_json

class Setup:
    def __init__(self, config_filename):
        self.config = read_json(config_filename)

    def plot_condition_number(self, task_data):
        cond_key = task_data["cond_key"]
        cond_data = self.config["tasks"][cond_key]
        sweep_key = cond_data["sweep_key"]
        sweep_data = self.config["tasks"][sweep_key]

        n = sweep_data["count"]
        min_value = sweep_data["min_value"]
        max_value = sweep_data["max_value"]
        k = (math.log(max_value) - math.log(min_value))/(n-1)
        m = math.log(min_value)
        weights = [math.exp(k*i + m) for i in range(sweep_data["count"])]
        cond_filename = cond_data["output_filename"]
        cond_numbers = read_json(cond_filename) # TEMPORARY FILENAME HACK!!!

        fig, ax = plt.subplots()
        ax.plot([x["weight"] for x in cond_numbers], [x["maxDiff"] for x in cond_numbers])
        ax.set_xscale('log')
        ax.set_xlabel("Weight")
        ax.set_ylabel('"Condition number"')
        plt.show()
        print("Plotted it")
        
    def perform_task(self, task_key):
        task_data = self.config["tasks"][task_key]
        task_type = task_data["type"]
        if task_type == "plot_condition_number":
            self.plot_condition_number(task_data)
        else:
            raise RuntimeError("Unknown task type: " + task_type)
        

#Setup("../../../data/demo_occupation_sni/config.json").perform_task("plot_condition_number")
